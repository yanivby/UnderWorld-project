#include "..\..\Includes\Handlers\InGameHandler.h"
#include "../../Includes/Backend/Serialization/JsonRequestPacketDeserializer.h"
#include "../../Includes/Backend/Serialization/JsonResponsePacketSerializer.h"
#include "../../Includes/Backend/json.hpp"

InGameHandler::InGameHandler(RequestHandlerFactory* reqHandlerFactory, Room curRoom, loggedUser usr) : reqHandlerFactory(reqHandlerFactory), curRoom(curRoom), curUser(usr)
{

}

bool InGameHandler::isRequestRelevent(RequestInfo r)
{
	return r.requestId == UPDATE_INGAME_STATE || r.requestId == LOGOUT_CODE;
}

IRequestHandler::RequestResult InGameHandler::handleRequest(RequestInfo r)
{
	if (isRequestRelevent(r))
	{
		RequestResult res;
		UpdatePlayerStateRequest StatesRes;
		usersStateInGameResponse resState;

		StatesRes = JsonRequestPacketDeserializer::desrializeUpdatePlayerStateRequest(r.buffer);

		reqHandlerFactory->getInGameManager()->UpdatePlayerState(curUser, vec2d(StatesRes.x, StatesRes.y), StatesRes.animation, StatesRes.action, StatesRes.curAction);
		auto allUserStates = reqHandlerFactory->getInGameManager()->getUsersInGame(curUser);

		switch (r.requestId)
		{
		case UPDATE_INGAME_STATE:
			
			for (const auto& each : allUserStates) {
				auto temp = userData();
				loggedUser tempusr = each.first;
				temp.username = tempusr.getUserName();
				temp.aliveState = each.second.alive;
				temp.animationState = each.second.animationState;
				temp.curUserRole = each.second.curRole;
				temp.x = each.second.location.x;
				temp.y = each.second.location.y;
				temp.curAction = each.second.curAction;
				temp.action = each.second.action;
				resState.allUsers.push_back(temp);
			}

			res.response = JsonResponsePacketSerializer::serializeRespones(resState);
			res.newHandler = NULL;
			return res;
			break;
		case LOGOUT_CODE:
			RequestResult res;
			logoutRespones logRes;
			reqHandlerFactory->getRoomManager()->deleteUser(curUser);
			reqHandlerFactory->getInGameManager()->leaveGame(curUser);

			logRes.status = MenuRequestHandler::returnCode::OK;
			res.response = JsonResponsePacketSerializer::serializeRespones(logRes);
			res.newHandler = reqHandlerFactory->createMenuRequestHandler(curUser);
			return res;
			

			/*errorRespones err;
			err.errorMsg = "user not in room";
			res.response = JsonResponsePacketSerializer::serializeRespones(err);
			res.newHandler = reqHandlerFactory->createMenuRequestHandler(curUser);
			return res;
			*/

			break;
		
		}
		
	}
	errorRespones er;
	er.errorMsg = "ERROR: invalid Operation";
	std::vector<unsigned char> msg = JsonResponsePacketSerializer::serializeRespones(er);
	IRequestHandler::RequestResult* res = new IRequestHandler::RequestResult;
	res->response = msg;
	res->newHandler = NULL;
	return *res;
}


