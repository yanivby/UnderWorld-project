#include "../../Includes/Handlers/MenuRequestHandler.h"
#include "../../Includes/Backend/Serialization/JsonRequestPacketDeserializer.h"
#include "../../Includes/Backend/Serialization/JsonResponsePacketSerializer.h"
#include "../../Includes/Backend/json.hpp"
IRequestHandler::RequestResult MenuRequestHandler::signout(RequestInfo r)
{
	RequestResult res;
	logoutRespones logRes;
	this->reqHandlerFactory->getRoomManager()->deleteUser(this->user);
	this->reqHandlerFactory->getInGameManager()->leaveGame(this->user);
	if (reqHandlerFactory->getLoginManager()->logout(this->user.getUserName()) == loginManager::returnCode::OK)
	{	
		logRes.status = MenuRequestHandler::returnCode::OK;
		res.response = JsonResponsePacketSerializer::serializeRespones(logRes);
		res.newHandler = reqHandlerFactory->createLoginRequestHandler();
		return res;
	}

	errorRespones err;
	err.errorMsg = "user already disconnected";
	res.response = JsonResponsePacketSerializer::serializeRespones(err);
	res.newHandler = reqHandlerFactory->createLoginRequestHandler();
	return res;
}

IRequestHandler::RequestResult MenuRequestHandler::getRooms(RequestInfo r)
{
	RequestResult res;
	getRoomsRespones getRoomRes;
	auto rooms = reqHandlerFactory->getRoomManager()->getRooms();
	std::vector<RoomData> roomsData;
	for (auto it = rooms.begin(); it!=rooms.end(); it++)
	{
		roomsData.push_back(it->second.getMetaData());
	}
	getRoomRes.rooms = roomsData;
	getRoomRes.status = MenuRequestHandler::returnCode::OK;
	res.response = JsonResponsePacketSerializer::serializeRespones(getRoomRes);
	res.newHandler = reqHandlerFactory->createMenuRequestHandler(user);
	return res;
}

IRequestHandler::RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo r)
{
	nlohmann::json data = JsonRequestPacketDeserializer::getJson(r.buffer);
	auto rooms = reqHandlerFactory->getRoomManager()->getRooms();
	RequestResult res;
	for (int i = 0; i <rooms.size(); i++)
	{
		if (rooms[i].getMetaData().id == data["roomId"])
		{
			getPlayersInRoomRespones playerData;
			std::vector<loggedUser> loggedUsersVec = rooms[i].getUsers();
			std::vector<std::string> userNames;
			for (std::vector<loggedUser>::iterator it = loggedUsersVec.begin(); it != loggedUsersVec.end(); ++it) {
				userNames.push_back(it->getUserName());
			}
			playerData.players = userNames;
			playerData.name = rooms[i].getRoomName();
			playerData.maxPlayers = rooms[i].getMetaData().maxPlayers;
			
			res.response = JsonResponsePacketSerializer::serializeRespones(playerData);
			res.newHandler = reqHandlerFactory->createMenuRequestHandler(user);
			return res;
		}
	}
	errorRespones err;
	err.errorMsg = "no such room exists";
	res.response = JsonResponsePacketSerializer::serializeRespones(err);
	res.newHandler = reqHandlerFactory->createMenuRequestHandler(user);
	return res;
}

/*IRequestHandler::RequestResult MenuRequestHandler::getStatistics(RequestInfo r)
{
	RequestResult res;
	getStatisticsRespones statRes;
	statRes.statistics = this->reqHandlerFactory->getStatisticsManger()->getAllStats(user);
	statRes.status = MenuRequestHandler::returnCode::OK;
	res.newHandler = reqHandlerFactory->createMenuRequestHandler(user);
	res.response = JsonResponsePacketSerializer::serializeRespones(statRes);
	return res;
}*/

IRequestHandler::RequestResult MenuRequestHandler::joinRoom(RequestInfo r)
{
	RequestResult res;
	errorRespones err;
	joinRoomRespones joinRes;
	nlohmann::json data = JsonRequestPacketDeserializer::getJson(r.buffer);
	res.newHandler = reqHandlerFactory->createMenuRequestHandler(user);
	auto rooms = reqHandlerFactory->getRoomManager()->getRooms();
	std::string name = data["roomName"];
	auto curRoom = reqHandlerFactory->getRoomManager()->FindRoomWithName(name);
	//FIX THIS FUNCTION SO YOU CAN ACTUALLY ADD USERS TO A ROOM BY NAME
	int id = -1;
	for (auto it = rooms.begin(); it != rooms.end(); it++)
	{
		if (it->second.getRoomName() == name)
		{
			id = it->first;
		}
	}
	if (id == -1)
	{
		err.errorMsg = "no such room ";
		res.response = JsonResponsePacketSerializer::serializeRespones(err);
		return res;
	}

	
	
	if (curRoom.getMetaData().maxPlayers > curRoom.getUsers().size())
	{
		reqHandlerFactory->getRoomManager()->addUser(id,this->user);
		Room curRoom = reqHandlerFactory->getRoomManager()->getRoomWithId(reqHandlerFactory->getRoomManager()->getRoomId(this->user));
		reqHandlerFactory->getInGameManager()->joinGame(this->user, curRoom);
		joinRes.status = MenuRequestHandler::returnCode::OK;
		res.response = JsonResponsePacketSerializer::serializeRespones(joinRes);
		res.newHandler = reqHandlerFactory->createInGameRequestHandler(curRoom, this->user);
		return res;
	}
	err.errorMsg = "room full";
	res.response = JsonResponsePacketSerializer::serializeRespones(err);
	return res;
	
	



	err.errorMsg = "room doesnt exist";
	res.response = JsonResponsePacketSerializer::serializeRespones(err);
	return res;
}

IRequestHandler::RequestResult MenuRequestHandler::createRoom(RequestInfo r)
{
	createRoomRespones createRes;
	RequestResult res;
	
	auto curRoomData = JsonRequestPacketDeserializer::desrializeCreateRoomRequest(r.buffer);


	auto rooms = reqHandlerFactory->getRoomManager()->getRooms();
	if (reqHandlerFactory->getRoomManager()->createRoom(this->user, curRoomData))
	{
		Room curRoom = reqHandlerFactory->getRoomManager()->getRoomWithId(reqHandlerFactory->getRoomManager()->getRoomId(this->user));
		reqHandlerFactory->getInGameManager()->CreateNewGame(curRoom);
		
		createRes.status = MenuRequestHandler::returnCode::OK;
		res.newHandler = reqHandlerFactory->createInGameRequestHandler(curRoom, this->user);
		res.response = JsonResponsePacketSerializer::serializeRespones(createRes);
		return res;
	}

	errorRespones err;
	err.errorMsg = "room already exists";
	res.response = JsonResponsePacketSerializer::serializeRespones(err);
	res.newHandler = reqHandlerFactory->createMenuRequestHandler(user);
	return res;
}

MenuRequestHandler::MenuRequestHandler(loggedUser user,  RequestHandlerFactory* reqHandlerFactory) : user(user), reqHandlerFactory(reqHandlerFactory){}

bool MenuRequestHandler::isRequestRelevent(RequestInfo r)
{
	return r.requestId >= LOGOUT_CODE && r.requestId <= GET_PLAYERS_CODE;
}

IRequestHandler::RequestResult MenuRequestHandler::handleRequest(RequestInfo r)
{

	switch (r.requestId)
	{
	case CREATE_ROOM_CODE:
		return createRoom(r);
		break;
	case GET_ROOM_CODE:
		return getRooms(r);
		break;
	case GET_PLAYERS_CODE:
		return getPlayersInRoom(r);
		break;
	case JOIN_ROOM_CODE:
		return joinRoom(r);
		break;
	//case GET_STATS_CODE:
		//return getStatistics(r);
		//break;
	case LOGOUT_CODE:
		return signout(r);
		break;
	default: 
		break;
	}
	//if it passed all of those codes its an invalid code, shouldnt get here.
	RequestResult res;
	errorRespones err;
	err.errorMsg = "invalid menu operation";
	res.response = JsonResponsePacketSerializer::serializeRespones(err);
	res.newHandler = reqHandlerFactory->createMenuRequestHandler(user);
	return res;
}

loggedUser MenuRequestHandler::getUser()
{
	return this->user;
}
