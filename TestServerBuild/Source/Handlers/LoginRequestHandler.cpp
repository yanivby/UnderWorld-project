#include "../../Includes/Handlers/LoginRequestHandler.h"
#include "../../Includes/Handlers/MenuRequestHandler.h"
#include "../../Includes/Backend/Serialization/JsonResponsePacketSerializer.h"
#include "../../Includes/Backend/Serialization/JsonRequestPacketDeserializer.h"
#include "../../Includes/Managers/LoginManager.h"


LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory* hf)
{
	this->handlerFactory = hf;
}

bool LoginRequestHandler::isRequestRelevent(RequestInfo r)
{
	if (r.requestId == LOGIN_CODE)
	{
		loginRequest l = JsonRequestPacketDeserializer::deserializeLoginRequest(r.buffer);
		if (l.password.empty() || l.userName.empty())
		{
			return false;
		}
	}
	else if (r.requestId == SIGNUP_CODE)
	{
		signupRequset s = JsonRequestPacketDeserializer::deserializeSignupRequest(r.buffer);
		if (s.password.empty() || s.userName.empty() )
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	return true;
}

IRequestHandler::RequestResult LoginRequestHandler::handleRequest(RequestInfo r)
{
	loginManager* login = this->handlerFactory->getLoginManager();
	if (isRequestRelevent(r))
	{
		if (r.requestId == LOGIN_CODE)
		{
			loginRequest l = JsonRequestPacketDeserializer::deserializeLoginRequest(r.buffer);
			int result = login->login(l.userName, l.password);

			if (result == loginManager::OK)// not switch case cause intelizing parmeter at switch case isn't possible
			{
				loginRespones lr;
				lr.status = 1;
				std::vector<unsigned char> msg = JsonResponsePacketSerializer::serializeRespones(lr);
				IRequestHandler::RequestResult* res = new IRequestHandler::RequestResult;
				res->response = msg;
				MenuRequestHandler* handle = handlerFactory->createMenuRequestHandler(loggedUser(l.userName));
				res->newHandler = handle;
				return *res;
			}
			else if (result == loginManager::ALREADY_LOGGED_IN)
			{
				errorRespones er;
				er.errorMsg = "ERROR: User allready logged in";
				std::vector<unsigned char> msg = JsonResponsePacketSerializer::serializeRespones(er);
				IRequestHandler::RequestResult* res = new IRequestHandler::RequestResult;
				res->response = msg;
				res->newHandler = handlerFactory->createLoginRequestHandler();
				return *res;
			}
			else if (result == loginManager::INVALID_PASS)
			{
				errorRespones er;
				er.errorMsg = "ERROR: incorrect password";
				std::vector<unsigned char> msg = JsonResponsePacketSerializer::serializeRespones(er);
				IRequestHandler::RequestResult* res = new IRequestHandler::RequestResult;
				res->response = msg;
				res->newHandler = handlerFactory->createLoginRequestHandler();
				return *res;
			}
			else if (result == loginManager::USER_DOES_NOT_EXIST)
			{
				errorRespones er;
				er.errorMsg = "ERROR: User doesn't exist";
				std::vector<unsigned char> msg = JsonResponsePacketSerializer::serializeRespones(er);
				IRequestHandler::RequestResult* res = new IRequestHandler::RequestResult;
				res->response = msg;
				res->newHandler = handlerFactory->createLoginRequestHandler();
				return *res;
			}
		}
		else// not switch case cause intelizing parmeter at switch case isn't possible
		{
			signupRequset s = JsonRequestPacketDeserializer::deserializeSignupRequest(r.buffer);

			int result = login->signup(s.userName, s.password, s.email);

			if (result == loginManager::OK)
			{
				singupRespones r;
				r.status = 1;
				std::vector<unsigned char> msg = JsonResponsePacketSerializer::serializeRespones(r);
				RequestResult* res = new RequestResult;
				res->response = msg;
				MenuRequestHandler* handle = handlerFactory->createMenuRequestHandler(loggedUser(s.userName));
				res->newHandler = handle;
				return *res;
			}
			else if (result == loginManager::USERNAME_TAKEN)
			{
				errorRespones er;
				er.errorMsg = "ERROR: Username allready exist";
				std::vector<unsigned char> msg = JsonResponsePacketSerializer::serializeRespones(er);
				IRequestHandler::RequestResult* res = new IRequestHandler::RequestResult;
				res->response = msg;
				res->newHandler = handlerFactory->createLoginRequestHandler();
				return *res;
			}
		}
	}
	else
	{
		errorRespones r;
		r.errorMsg = "Invalid request";
		std::vector<unsigned char> msg = JsonResponsePacketSerializer::serializeRespones(r);
		RequestResult* res = new RequestResult;
		res->response = msg;
		res->newHandler = handlerFactory->createLoginRequestHandler();
		return *res;
	}
}