#include "../../Includes/Handlers/RequestHandlerFactory.h"


RequestHandlerFactory::RequestHandlerFactory(IDatabase* db)
{
	this->database = db;
	this->logMan = new loginManager(db);
	this->roomMan = new RoomManager();
	this->statMan = new StatisticsManager(db);
	this->InGameMan = new InGameManager();
}

RequestHandlerFactory::~RequestHandlerFactory()
{
	delete logMan;
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	LoginRequestHandler* log = new LoginRequestHandler(this);
	return log;
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(loggedUser user)
{
	MenuRequestHandler* menu = new MenuRequestHandler(user,this);
	return menu;
}

InGameHandler* RequestHandlerFactory::createInGameRequestHandler(Room room, loggedUser usr)
{
	InGameHandler* inGameHandler = new InGameHandler(this,room,usr);
	return inGameHandler;
}



loginManager* RequestHandlerFactory::getLoginManager()
{
	return this->logMan;
}

RoomManager* RequestHandlerFactory::getRoomManager()
{
	return roomMan;
}


InGameManager* RequestHandlerFactory::getInGameManager()
{
	return InGameMan;
}

/*StatisticsManager* RequestHandlerFactory::getStatisticsManger()
{
	return statMan;
}*/
