#include "../../Includes/Managers/LoginManager.h"

loginManager::loginManager(IDatabase* m_dataBase): m_dataBase(m_dataBase){}



loginManager::returnCode loginManager::signup(std::string username, std::string password, std::string email)
{
	if (m_dataBase->doesUserExist(username))
	{
		return USERNAME_TAKEN;
	}	
	bool res = m_dataBase->addNewUser(username, password, email);
	m_loggedUsers.push_back(loggedUser(username));
	return OK;
}

loginManager::returnCode loginManager::login(std::string username, std::string password)
{
	bool res = m_dataBase->doesPasswordMatch(username, password);
	if (!m_dataBase->doesUserExist(username))
	{
		return USER_DOES_NOT_EXIST;
	}
	if (res == false && m_dataBase->doesUserExist(username))
	{
		return INVALID_PASS;
	}
	if (isUserLogged(username))
	{
		return ALREADY_LOGGED_IN;
	}
	m_loggedUsers.push_back(loggedUser(username));
	return OK;
}

loginManager::returnCode loginManager::logout(std::string username)
{
	if (isUserLogged(username))
	{
		m_loggedUsers.erase(std::find(m_loggedUsers.begin(), m_loggedUsers.end(), loggedUser(username)));
		return OK;
	}
	return USERNAME_NOT_CONNECTED;
}

bool loginManager::isUserLogged(std::string username)
{
	return std::find(m_loggedUsers.begin(), m_loggedUsers.end(), loggedUser(username)) != m_loggedUsers.end();
}

loggedUser::loggedUser(std::string username): username(username){}

std::string loggedUser::getUserName()
{
	return this->username;
}

void loggedUser::setUsername(std::string username)
{
	this->username = username;
}

bool loggedUser::operator==(const loggedUser other)
{
	return this->username == other.username;
}



