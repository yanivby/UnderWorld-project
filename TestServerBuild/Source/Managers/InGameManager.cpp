#include "../../Includes/Managers/InGameManager.h"



InGameManager::InGameManager()
{
}

InGameManager::~InGameManager()
{
}

void InGameManager::UpdatePlayerState(loggedUser user, vec2d loc, int animation, int acc, std::string curAction)
{
	int gameId = findGameWithUser(user);
	games[gameId].UpdatePlayerState(user, loc, animation, acc, curAction);

}

std::map<loggedUser, PlayerState> InGameManager::getUsersInGame(loggedUser usr)
{
	int gameId = findGameWithUser(usr);
	return games[gameId].getAllUsersData();
}

void InGameManager::CreateNewGame(Room curRoom)
{
	games[(curRoom).getMetaData().id] = Game(curRoom);
}

bool InGameManager::joinGame(loggedUser usr, Room curRoom)
{
	int gameId = -1;
	for (auto& each : games) {
		if ((each.second.curRoom.getMetaData().name) == curRoom.getMetaData().name)
		{
			gameId = each.first;
		}
	}
	if (gameId !=-1)
	{
		games[gameId].addUser(usr);
		return true;
	}
	return false;
}



bool InGameManager::leaveGame(loggedUser usr)
{
	int gameId = findGameWithUser(usr);
	if (gameId != -1)
	{
		games[gameId].removeUser(usr);
		return true;
	}
	return false;
	
}


int InGameManager::findGameWithUser(loggedUser usr)
{
	for (const auto& each : games) {
	
		if (each.second.playerStates.count(usr) != 0)
		{
			return each.first;
		}
	}
	return -1;
}


vec2d::vec2d(float x, float y) : x(x), y(y) {}


PlayerState::PlayerState():
	taskList(std::vector<Task>{ Task(CATCH_THE_ORE), Task(MINE_THE_ORE), Task(SORT_THE_ORE) }),
	recentlyMoved(false),
location(DEFAULT_START_X_VAL, DEFAULT_START_Y_VAL),
alive(1)
{
	curRole = 0;
	animationState = 0;
	curAction = "";
	
}

PlayerState::PlayerState(std::vector<Task> tasks, int role) :
taskList(tasks),
recentlyMoved(false),
location(DEFAULT_START_X_VAL, DEFAULT_START_Y_VAL),
alive(1)
{
	curRole = role;
	animationState = 0;
	curAction = "";
}


#pragma region Game


Game::Game(Room curRoom) :curRoom (curRoom)
{
	gameStarted = false;
	for (loggedUser each : curRoom.getUsers())
	{
		//add rondom imposter;
		//need to make sure not all of the players are impostors \ mineres to have a normal balance
		playerStates.insert({ each, PlayerState(std::vector<Task>{ Task(CATCH_THE_ORE), Task(MINE_THE_ORE), Task(SORT_THE_ORE) }, 0) });
	}	
}

void Game::UpdatePlayerState(loggedUser user, vec2d loc,int animation, int acc , std::string curAction)
{
	if (!playerStates[user].recentlyMoved)
	{
		if (playerStates[user].alive == 1)
			playerStates[user].location = loc;
	
	}
	playerStates[user].recentlyMoved = false;

	playerStates[user].animationState = animation;
	playerStates[user].curAction = curAction;
	if (playerStates[user].action == 50)
		playerStates[user].action = 0;
	int taskId = 0;
	switch (acc)
	{
	case MOVE:
		break;
	case KILL:
		killUser(loggedUser(curAction) );
		break;
	case START_GAME:
		startGame();
		break;
	case DO_TASK:
		taskId = (atoi(curAction.c_str()));
		DoTask(user, static_cast<TASKS>(taskId) );
		break;
	default:
		break;
	}

}

std::map<loggedUser, PlayerState> Game::getAllUsersData()
{
	return playerStates;

}

void Game::addUser(loggedUser usr)
{
	auto tasks = std::vector<Task>{ Task(CATCH_THE_ORE), Task(MINE_THE_ORE), Task(SORT_THE_ORE) };
	playerStates[usr] = PlayerState(tasks, rand() % 2);
}

bool Game::removeUser(loggedUser usr)
{
	try
	{
		playerStates.erase(usr);
		return true;

	}
	catch (const std::exception&)
	{
		return false;
	}
	return false;

}



void Game::checkWin()
{
	int alivePlayers = 0;
	int tasksDone = 0;
	int playerCount = 0;

	

	for (auto it = playerStates.begin(); it != playerStates.end(); it++)
	{
		alivePlayers += it->second.alive&& it->second.curRole == 0 ? 1 : 0;
		playerCount += it->second.curRole == 0 ? 1 : 0;
		for (auto each : it->second.taskList)
		{
			tasksDone += each.Done ? 1 : 0;
		}
		
	}

	if (tasksDone == playerCount * 2)
	{
		minerWin();
	}
	else if (alivePlayers == 0)
	{
		impostorsWin();
	}

	
}
void Game::startGame()
{
	if (playerStates.size() > 1)
	{
		int killerIndex = rand() % playerStates.size();
		int counter = 0;

		for (auto it = playerStates.begin(); it != playerStates.end(); it++)
		{
			it->second.recentlyMoved = true;
			if (killerIndex == counter)
			{
				it->second.curRole = 1;
				it->second.curAction = "MOVE";
				it->second.action = 50;
				it->second.location.x = 1800 ;
				it->second.location.y = 3600 ;
				it->second.alive = true;
			}
			else
			{
				int randIndex = rand() % SPAWN_POINT_COUNT;
				it->second.curRole = 0;
				it->second.curAction = "MOVE";
				it->second.action = 50;
				it->second.location.x = spawnPoints[randIndex][0];
				it->second.location.y = spawnPoints[randIndex][1];
				it->second.alive = true;
			}
			counter++;

			
		}
	}
	

	//need to move all users to one of the 6 predetermined spawn locations.V
}
void Game::minerWin()
{

	for (auto it = playerStates.begin(); it != playerStates.end(); it++)
	{
		it->second.alive = true;
		it->second.curAction = "MINERS WIN!";
		it->second.action = 100;
	}
	//need to update actions to code 100 with name fo victors
}

void Game::impostorsWin()
{
	//need to update actions to code 200 with name fo victors
	for (auto it = playerStates.begin(); it != playerStates.end(); it++)
	{
		it->second.alive = true;
		it->second.curAction = "IMPOSTORS WIN!";
		it->second.action = 200;
	}
}



void Game::DoTask(loggedUser usr, TASKS taskId)
{
	playerStates[usr].taskList[taskId].Done = true;
	checkWin();
}

void Game::killUser(loggedUser user)
{
	try {
		playerStates[user].alive = 0;
		checkWin();
	}
	catch (const std::exception&)
	{
			
	}


}
#pragma endregion
