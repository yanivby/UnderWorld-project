#include "../../Includes/Managers/RoomManager.h"
#include <exception>
#include <string>

#define MAX_ROOM_NUM 100
#define ERROR -1

bool RoomManager::createRoom(loggedUser user, createRoomRequest data)
{
	bool foundFlag = true;
	unsigned int id = 0;
	for (unsigned int i = 0; i < MAX_ROOM_NUM && foundFlag; ++i)
	{
		if (this->rooms.find(i) == this->rooms.end())
		{
			id = i;
			foundFlag = false;
		}
	}
	if (!foundFlag)
	{
		RoomData* rData = new RoomData();
		rData->id = id;
		rData->isActive = 1;
		rData->maxPlayers = data.maxUsers;
		rData->name = data.roomName;
		
		
		Room* r = new Room(*rData);
		r->addUser(user);

		this->rooms.insert(std::pair<unsigned int, Room>(id, *r));
		return true;
	}
	else
	{
		throw "Not enough room space";
	}
	return false;
}

void RoomManager::deleteRoom(unsigned int ID)
{
	if (this->rooms.find(ID) != this->rooms.end())
	{
		this->rooms.erase(ID);
	}
}

unsigned int RoomManager::getRoomState(unsigned int ID)
{
	removeEmptyRooms();
	if (this->rooms.find(ID) != this->rooms.end())
	{
		Room r = this->rooms[ID];
		RoomData rmDt = r.getMetaData();
		return rmDt.isActive;
	}
	return ERROR;
}

std::map<unsigned int, Room> RoomManager::getRooms()
{	
	removeEmptyRooms();
	return this->rooms;
}

void RoomManager::addUser(int id,loggedUser user)
{
	removeEmptyRooms();
	rooms[id].addUser(user);
}

int RoomManager::getRoomId(loggedUser usr)
{
	removeEmptyRooms();
	for (auto it = rooms.begin(); it != rooms.end(); ++it)
	{
		auto cur = it->second.getUsers();
		if (count(cur.begin(), cur.end(), usr)!=0)
		{
			return it->second.getMetaData().id;
		}
	}
	return ERROR;

}
Room RoomManager::FindRoomWithName(std::string roomName)
{
	for (auto it = this->rooms.begin(); it != this->rooms.end(); it++)
	{
		if (it->second.getRoomName() == roomName)
			return it->second;
	}
	
}
Room RoomManager::getRoomWithId(int id)
{
	if (this->rooms.find(id) != this->rooms.end())
	{
		return this->rooms[id];
	}
	return Room();
}

void RoomManager::removeUser(int id, loggedUser user)
{
	rooms[id].removeUser(user);
	if (rooms[id].getUsers().size() == 0)
	{
		deleteRoom(id);
	}
	
	removeEmptyRooms();
}

void RoomManager::removeEmptyRooms()
{

	std::list<int> toRemove;
	for (auto x : rooms)
	{
		if (x.second.getUsers().size() == 0)
		{
			toRemove.push_back(x.first);
		}
	}
	for (auto x : toRemove)
	{
		deleteRoom(x);
	}
}

void RoomManager::deleteUser(loggedUser user)
{

	for (auto i = this->rooms.begin(); i != this->rooms.end(); ++i)
	{
		try
		{
			i->second.removeUser(user);
		}
		catch (std::exception e)
		{

		}
	}
	removeEmptyRooms();
}