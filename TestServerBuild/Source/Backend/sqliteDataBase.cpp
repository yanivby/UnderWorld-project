#include "../../Includes/Backend/sqliteDataBase.h"
#include <iostream>
#include "../../Includes/Backend/sqlite3.h"
#include <io.h>
#include <fstream>
sqliteDataBase::sqliteDataBase()
{
	int doesFileExist = _access(DB_PATH, 0);
	int res = sqlite3_open(DB_PATH, &DB);
	if (res != SQLITE_OK)
	{
		DB = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		throw "error opening db.";
	}
	if (doesFileExist != 0)
	{
		std::cout << "creating new tables"<<std::endl;
		char* errMessage = nullptr;
		res = sqlite3_exec(DB, "CREATE TABLE USERS (ID INTEGER PRIMARY KEY AUTOINCREMENT, USERNAME TEXT NOT NULL, PASSWORD TEXT NULL, EMAIL TEXT NOT NULL); ", nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
			throw "error creating table";
		/*res = sqlite3_exec(DB, "CREATE TABLE QUESTIONS (ID INTEGER PRIMARY KEY AUTOINCREMENT, QUESTION TEXT NOT NULL, CORRECT_ANSWER TEXT NULL, INCORRECT_ANSWER1 TEXT NULL, INCORRECT_ANSWER2 TEXT NULL,INCORRECT_ANSWER3 TEXT NULL); ", nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
			throw "error creating table";
		*/
		/*res = sqlite3_exec(DB, "CREATE TABLE STATISTICS (ID INTEGER , AVG_ANSWER_TIME FLOAT(8,4), CORRECT_ANSWERS INTEGET, TOTAL_ANSWERS INTEGER, GAMES_PLAYED INTEGER, FOREIGN KEY (ID) REFERENCES  USERS(ID)); ", nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
			throw "error creating table";
		*/
		//loadQuestionsFromJson(getJsonFromFile("QUESTIONS.txt"));
	}
	std::cout << "Opened DB" << std::endl;

}
sqliteDataBase::~sqliteDataBase()
{
	int res = 0;
	res = sqlite3_close(DB);
	if (res != SQLITE_OK)
		throw "error Closing table";
}
bool sqliteDataBase::doesUserExist(std::string username)
{
	char* errMessage = nullptr;
	bool result = false;
	std::string q = "SELECT * FROM USERS WHERE USERNAME = '" + username + "';";
	int res = sqlite3_exec(DB, q.c_str(), callbackUserExists, &result, &errMessage);
	if (res != SQLITE_OK)
		throw "ERROR REACHING TABLE";
	return result;
}

bool sqliteDataBase::doesPasswordMatch(std::string username, std::string password)
{
	char* errMessage = nullptr;
	bool result = false;
	std::string q = "SELECT * FROM USERS WHERE USERNAME = '" + username + "' AND PASSWORD = '" + password + "' ;";
	int res = sqlite3_exec(DB, q.c_str(), callbackUserExists, &result, &errMessage);
	if (res != SQLITE_OK)
		throw "ERROR REACHING TABLE";
	return result;
}

bool sqliteDataBase::addNewUser(std::string username, std::string password, std::string email)
{
	if (!doesUserExist(username))
	{
		char* errMessage = nullptr;
		bool result = false;
		std::string q = "INSERT INTO USERS (USERNAME, PASSWORD, EMAIL) values ('" + username + "','" + password + "','" + email + "')";
		int res = sqlite3_exec(DB, q.c_str(), NULL, NULL, &errMessage);
		if (res != SQLITE_OK)
			throw "ERROR REACHING TABLE";
		return true;
	}
	return false;
}
/*
std::list<question> sqliteDataBase::getQuestions(int numOfQuestions)
{
	std::list<question> result;
	char* errMessage = nullptr;
	for (int i = 1; i <= numOfQuestions; i++)
	{
		std::string q = "SELECT * FROM QUESTIONS WHERE ID = " + std::to_string(i) + ";";
		int res = sqlite3_exec(DB, q.c_str(), callbackQuestions, &result, &errMessage);
		if (res != SQLITE_OK)
			throw "ERROR REACHING TABLE";
	}
	return result;
}

float sqliteDataBase::PlayerAvarageAnswerTime(std::string username)
{
	statistics* stats =  getStatistics(username);
	float res = stats->avgAnswerTime;
	delete stats;
	return res;
}

int sqliteDataBase::getNumOfCorrectAnswers(std::string username)
{
	statistics* stats = getStatistics(username);
	int res = stats->correctAnswerCount;
	delete stats;
	return res;
}

int sqliteDataBase::getNumOfTotalAnswers(std::string username)
{
	statistics* stats = getStatistics(username);
	int res = stats->totalAnswerCount;
	delete stats;
	return res;
}*/

/*int sqliteDataBase::getNumOfPlayerGames(std::string username)
{
	statistics* stats = getStatistics(username);
	int res = stats->playerGameCount;
	delete stats;
	return res;
}*/
/*
void sqliteDataBase::loadQuestionsFromJson(nlohmann::json questions)
{
	//std::cout << questions.dump(4) << std::endl;
	std::list<question> result;
	char* errMessage = nullptr;

	for (size_t i = 0; questions["results"][i] != nullptr; i++)
	{
		std::string q = "INSERT INTO QUESTIONS (QUESTION, CORRECT_ANSWER, INCORRECT_ANSWER1, INCORRECT_ANSWER2, INCORRECT_ANSWER3) values ('" + std::string(questions["results"][i]["question"]) + "','" + std::string(questions["results"][i]["correct_answer"]) + "','" + std::string(questions["results"][i]["incorrect_answers"][0]) +"','" + std::string(questions["results"][i]["incorrect_answers"][1]) + "','" + std::string(questions["results"][i]["incorrect_answers"][2]) + "');";
		//std::cout << q << std::endl;
		int res = sqlite3_exec(DB, q.c_str(), NULL, NULL, &errMessage);
		if (res != SQLITE_OK)
		{
			std::cout << errMessage << std::endl;
			throw "ERROR REACHING TABLE";
		}
	}
}*/

std::vector<std::string> sqliteDataBase::getAllUsers()
{
	std::vector<std::string> result;
	char* errMessage = nullptr;
	std::string q = "SELECT * FROM users;";
	int res = sqlite3_exec(DB, q.c_str(), callbackUser, &result, &errMessage);
	if (res != SQLITE_OK)
		throw "ERROR REACHING TABLE";
	return result;
}

nlohmann::json sqliteDataBase::getJsonFromFile(std::string fileName)
{
	std::ifstream file(fileName);
	if (file.fail())
		throw "ERROR OPENING FILE";
	std::string data((std::istreambuf_iterator<char>(file)),
		std::istreambuf_iterator<char>());
	file.close();
	return nlohmann::json::parse(data);
}

/*statistics* sqliteDataBase::getStatistics(std::string username)
{
	char* errMessage = nullptr;
	statistics *result = new statistics(-1, -1, -1, -1);
	std::string q = "SELECT * FROM STATISTICS INNER JOIN USERS ON USERS.ID = STATISTICS.ID WHERE USERS.USERNAME = '" + username + "';";
	int res = sqlite3_exec(DB, q.c_str(), callbackStatistics, result, &errMessage);
	if (res != SQLITE_OK)
		throw "ERROR REACHING TABLE";
	return result;  
}*/

int callbackUserExists(void* data, int argc, char** argv, char** azColName)
{
	*((bool*)data) = true;
	return 0;
}

/*int callbackQuestions(void* data, int argc, char** argv, char** azColName)
{
	question q(argv[1], argv[2],argv[3],argv[4],argv[5]);
	((std::list<question>*)data)->push_back(q);
	return 0;
}*/

/*int callbackStatistics(void* data, int argc, char** argv, char** azColName)
{
	delete(((statistics*)(data)));
	statistics* stats = new statistics((float)*argv[1], (int)argv[2], (int)argv[3], (int)argv[4]);
	*((statistics*)(data))= *stats;
	return 0;
}*/

int callbackUser(void* data, int argc, char** argv, char** azColName)
{
	((std::vector<std::string>*)data)->push_back(argv[1]);
	return 0;
}



