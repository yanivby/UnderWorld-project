#pragma once
#include "../../Includes/Backend/UnderWorldServer.h"
#include <exception>
#include <iostream>
#include <string>
#include <numeric>
#include "../../Includes/Backend/Serialization/JsonRequestPacketDeserializer.h"
#include "../../Includes/Backend/Serialization/JsonResponsePacketSerializer.h"
#include "../../Includes/Managers/LoginManager.h"
#include "../../Includes/Handlers/RequestHandlerFactory.h"
#include "../../Includes/Backend/sqliteDataBase.h"
#include "../../Includes/Backend/Helper.h"
#include <ctime>
#include <typeinfo>
#include <ws2tcpip.h>
#include <fstream>
// using static const instead of macros 
static const unsigned short PORT = 8826;
static const unsigned int IFACE = 0;


#define BUFFER_LEN 5
#define HEADER_BUFFER_LEN 5


UnderWorldServer::UnderWorldServer()
{
	// notice that we step out to the global namespace
	// for the resolution of the function socket
	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

	this->db = new sqliteDataBase();
	this->factory = new RequestHandlerFactory(db);
}

UnderWorldServer::~UnderWorldServer()
{
	std::cout << __FUNCTION__ " closing accepting socket" << std::endl;
	// why is this try necessarily ?
	delete db;
	delete factory;
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_socket);
	}
	catch (...) {}
}

void UnderWorldServer::serve()
{
	bindAndListen();
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "accepting client..."<< std::endl;
		acceptClient();
	}
}


// listen to connecting requests from clients
// accept them, and create thread for each client
void UnderWorldServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	//std::ifstream configFile ("config.txt", std::ios::in);
	std::string ipaddr = "127.0.0.1";
	//getline(configFile, ipaddr);
	//configFile.close();
	InetPton(AF_INET, (PCWSTR)(ipaddr.c_str()), (&sa.sin_addr));

	//	sa.sin_addr.s_addr = IFACE;
	// again stepping out to the global namespace
	if (::bind(_socket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	std::cout << "binded" << std::endl;

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	
	std::cout << "listening..." << std::endl;

}

void UnderWorldServer::acceptClient()
{
	SOCKET client_socket = accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted !" << std::endl;

	// create new thread for client	and detach from it
	std::thread tr(&UnderWorldServer::clientHandler, this, client_socket);
	tr.detach();

	LoginRequestHandler p(this->factory);
	users.insert({ client_socket, p });


}

void UnderWorldServer::clientHandler(SOCKET client_socket)
{
	loggedUser user("");
	try
	{
		LoginRequestHandler* loginHandler = new LoginRequestHandler(this->factory);
		IRequestHandler*handler = loginHandler;
		while (true)
		{
			char* header = Helper::getPartFromSocket(client_socket, HEADER_BUFFER_LEN);
			if (header[0] == 0)
				throw "error reciving data";
			std::vector<uint8_t> headerVec(header, header + HEADER_BUFFER_LEN);
			delete[] header;

			int codeId = headerVec[0];
			unsigned int size = JsonRequestPacketDeserializer::buffToInteger(headerVec);

			char* data = Helper::getPartFromSocket(client_socket, size);
			if (data[0] == 0 && size !=0 )
				throw "error reciving data";
			std::vector<uint8_t> dataVec(data, data + size);
			if (size != 0)
				delete[] data;
			RequestInfo req(codeId, std::time(nullptr), dataVec);
			IRequestHandler::RequestResult reqRes = handler->handleRequest(req);
			if (dynamic_cast<MenuRequestHandler*>(reqRes.newHandler) != 0 && dynamic_cast<LoginRequestHandler*>(handler) != 0)//checking if handler changed to know if logging in was succesfull , if so we can save the name as logged on.
			{
				user.setUsername(JsonRequestPacketDeserializer::deserializeLoginRequest(dataVec).userName);
			}
			char* responsMsg = new char[reqRes.response.size()];
			std::copy(reqRes.response.begin(), reqRes.response.end(), responsMsg);
			//std::cout << reqRes.response.size() << std::endl;
			Helper::sendData(client_socket, responsMsg, reqRes.response.size());
			if (reqRes.newHandler != nullptr )
				handler = reqRes.newHandler;
		}
	}
	catch (const std::exception & e)
	{

		std::cout << "Exception was catch in function clientHandler. socket=" << client_socket << ", what=" << e.what() << std::endl;
		try
		{
			if (user.getUserName() != "")
			{

				MenuRequestHandler* menuRequest = this->factory->createMenuRequestHandler(user);
				std::vector<byte> ve;
				RequestInfo r(LOGOUT_CODE, std::time(nullptr), ve);
				menuRequest->handleRequest(r);
				user.setUsername("");
			}

		}
		catch (std::exception e)
		{

		}
	}
	users.erase(client_socket);
}
