#include "../../../Includes/Backend/Serialization/JsonRequestPacketDeserializer.h"

loginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::vector<unsigned char> buffer)
{
	loginRequest req;
	try
	{
		nlohmann::json j = getJson(buffer);
		req.userName = j["username"];
		req.password = j["password"];
	}
	catch(std::exception & e)
	{
		std::cout << e.what() << std::endl;
	}
	return req;
}

signupRequset JsonRequestPacketDeserializer::deserializeSignupRequest(std::vector<unsigned char> buffer)
{
	signupRequset req;
	try
	{
	nlohmann::json j = getJson(buffer);
	req.userName = j["username"];
	req.password = j["password"];
	//req.email = j["email"];
	req.email = "NOEMAIL";

	}
	catch (std::exception & e)
	{
		std::cout << e.what() << std::endl;
	}
	return req;
}

getPlayerInRoomRequest JsonRequestPacketDeserializer::desrializeGetInRoomRequest(std::vector<unsigned char> buffer)
{
	getPlayerInRoomRequest req;
	try
	{
		nlohmann::json j = getJson(buffer);
		req.roomId = j["roomId"];
	}
	catch(std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	return req;
}

joinRoomRequest JsonRequestPacketDeserializer::desrializeJoinRoomRequest(std::vector<unsigned char> buffer)
{
	joinRoomRequest req;
	try
	{
		nlohmann::json j = getJson(buffer);
		req.roomId = j["roomId"];
	}
	catch (std::exception & e)
	{
		std::cout << e.what() << std::endl;
	}
	return req;
}

createRoomRequest JsonRequestPacketDeserializer::desrializeCreateRoomRequest(std::vector<unsigned char> buffer)
{
	createRoomRequest req;
	try
	{
		//"{\"roomName\":\"yanivby's room\",\"maxUsers\":10,\"questionCount\":10,\"answerTimeout\":6}"
		nlohmann::json j = getJson(buffer);
		req.roomName = j["roomName"];
		req.maxUsers = j["maxUsers"];
		
	}
	catch (std::exception & e)
	{
		std::cout << e.what() << std::endl;
	}
	return req;
}

UpdatePlayerStateRequest JsonRequestPacketDeserializer::desrializeUpdatePlayerStateRequest(std::vector<unsigned char> buffer)
{
	UpdatePlayerStateRequest req;
	try
	{
		//"{\"roomName\":\"yanivby's room\",\"maxUsers\":10,\"questionCount\":10,\"answerTimeout\":6}"
		nlohmann::json j = getJson(buffer);
		req.x = j["x"];
		req.y = j["y"];
		req.animation =j["animation"];
		req.action = j["action"];
		req.curAction = j["curaction"];

	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	return req;
}


//perhaps not needed
int JsonRequestPacketDeserializer::buffToInteger(std::vector<unsigned char> buffer)
{
	int a = static_cast<int>(static_cast<unsigned char>(buffer[1]) << 24 |
		static_cast<unsigned char>(buffer[2]) << 16 |
		static_cast<unsigned char>(buffer[3]) << 8 |
		static_cast<unsigned char>(buffer[4]));
	return a;
}

nlohmann::json JsonRequestPacketDeserializer::getJson(std::vector<unsigned char> buffer)
{
	//int length = buffToInteger(buffer);
	std::string data(buffer.begin(), buffer.end() ); 
	return nlohmann::json::parse(data);
}
