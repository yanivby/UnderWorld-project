#include "../../../Includes/Backend/Serialization/JsonResponsePacketSerializer.h"
#include "../../../Includes/Backend/json.hpp"

enum RESPONES
{
	LOGIN = 1, SINGUP = 2, LOGOUT, JOIN_ROOM, CREATE_ROOM, GET_ROOM, GET_STATS, GET_PLAYERS,GET_INGAME_STATES, ERROR = 99
};
#define STATS_NUM 4
#define INT_LEN 4
#define SCORE_TOP 3
#define BYTE_SIZE 8

std::vector<unsigned char> JsonResponsePacketSerializer::serializeRespones(errorRespones e)
{
	std::vector<unsigned char> toSend;
	toSend.push_back((unsigned char)ERROR);

	nlohmann::json j;
	j["message"] = e.errorMsg;
	std::string errMsg = j.dump();

	std::vector<unsigned char> errM;
	std::copy(errMsg.begin(), errMsg.end(), std::back_inserter(errM));

	int len = errMsg.length();
	std::vector <unsigned char>lenv = convertToVector(len);

	toSend.insert(toSend.end(), lenv.begin(), lenv.end());//adding lenth to vector

	toSend.insert(toSend.end(), errM.begin(), errM.end());//adding error msg to vector


	return toSend;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeRespones(loginRespones l)
{
	std::vector<unsigned char> toSend;
	toSend.push_back((unsigned char)LOGIN);

	nlohmann::json j;
	j["status"] = l.status;
	std::string loginAnsS = j.dump();

	std::vector<unsigned char> loginAnsv;
	std::copy(loginAnsS.begin(), loginAnsS.end(), std::back_inserter(loginAnsv));

	int len = loginAnsS.length();
	std::vector <unsigned char>lenv = convertToVector(len);

	toSend.insert(toSend.end(), lenv.begin(), lenv.end());//adding lenth to vector

	toSend.insert(toSend.end(), loginAnsv.begin(), loginAnsv.end());//adding login res to vector


	return toSend;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeRespones(singupRespones s)
{
	std::vector<unsigned char> toSend;
	toSend.push_back((unsigned char)SINGUP);

	nlohmann::json j;
	j["status"] = s.status;
	std::string singupAnsS = j.dump();

	std::vector<unsigned char> singupAnsV;
	std::copy(singupAnsS.begin(), singupAnsS.end(), std::back_inserter(singupAnsV));

	int len = singupAnsS.length();

	std::vector <unsigned char>lenv = convertToVector(len);

	toSend.insert(toSend.end(), lenv.begin(), lenv.end());//adding lenth to vector

	toSend.insert(toSend.end(), singupAnsV.begin(), singupAnsV.end());//adding singup res to vector


	return toSend;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeRespones(logoutRespones l)
{
	std::vector<unsigned char> toSend;
	toSend.push_back((unsigned char)LOGOUT);

	nlohmann::json j;
	j["status"] = l.status;
	std::string logoutMsgS = j.dump();

	std::vector<unsigned char> logoutMsgV;
	std::copy(logoutMsgS.begin(), logoutMsgS.end(), std::back_inserter(logoutMsgV));

	int len = logoutMsgS.length();
	std::vector <unsigned char>lenv = convertToVector(len);

	toSend.insert(toSend.end(), lenv.begin(), lenv.end());//adding lenth to vector

	toSend.insert(toSend.end(), logoutMsgV.begin(), logoutMsgV.end());//adding logout msg to vector


	return toSend;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeRespones(joinRoomRespones r)
{
	std::vector<unsigned char> toSend;
	toSend.push_back((unsigned char)JOIN_ROOM);

	nlohmann::json j;
	j["status"] = r.status;
	std::string joinRommMsgS = j.dump();

	std::vector<unsigned char> joinRoomMsgV;
	std::copy(joinRommMsgS.begin(), joinRommMsgS.end(), std::back_inserter(joinRoomMsgV));

	int len = joinRommMsgS.length();
	std::vector <unsigned char>lenv = convertToVector(len);


	toSend.insert(toSend.end(), lenv.begin(), lenv.end());//adding lenth to vector

	toSend.insert(toSend.end(), joinRoomMsgV.begin(), joinRoomMsgV.end());//adding joinRoom msg to vector

	return toSend;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeRespones(createRoomRespones c)
{
	std::vector<unsigned char> toSend;
	toSend.push_back((unsigned char)CREATE_ROOM);

	nlohmann::json j;
	j["status"] = c.status;
	std::string MsgS = j.dump();

	std::vector<unsigned char> MsgV;
	std::copy(MsgS.begin(), MsgS.end(), std::back_inserter(MsgV));

	int len = MsgS.length();
	std::vector <unsigned char>lenv = convertToVector(len);

	toSend.insert(toSend.end(), lenv.begin(), lenv.end());//adding lenth to vector

	toSend.insert(toSend.end(), MsgV.begin(), MsgV.end());//adding create room msg to vector

	return toSend;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeRespones(getRoomsRespones r)
{
	std::vector<unsigned char> toSend;
	toSend.push_back((unsigned char)GET_ROOM);

	nlohmann::json j;
	j["status"] = r.status;
	std::vector<RoomData> rooms = r.rooms;
	std::string roomsName;
	for (auto i = rooms.begin(); i != rooms.end(); ++i)
	{
		roomsName.append(i->name);
		roomsName.append(" - ( " + std::to_string(i->activeUsersCount) );
		roomsName.append(" / " + std::to_string(i->maxPlayers) );
		roomsName.append(" )");
		roomsName.append(",");
	}
	roomsName = roomsName.substr(0, roomsName.length() - 1);
	j["rooms"] = roomsName;
	std::string MsgS = j.dump();

	std::vector<unsigned char> MsgV;
	std::copy(MsgS.begin(), MsgS.end(), std::back_inserter(MsgV));

	int len = MsgS.length();
	std::vector <unsigned char>lenv = convertToVector(len);

	toSend.insert(toSend.end(), lenv.begin(), lenv.end());//adding lenth to vector

	toSend.insert(toSend.end(), MsgV.begin(), MsgV.end());//adding rooms to vector

	return toSend;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeRespones(getStatisticsRespones s)
{
	std::vector<unsigned char> toSend;
	toSend.push_back((unsigned char)GET_STATS);

	nlohmann::json j;
	j["status"] = s.status;
	std::vector<std::string> stats = s.statistics;
	int count = 0;

	std::string scoreBoard;
	int padding = SCORE_TOP - (stats.size() - STATS_NUM);

	for (int i = 0; i < SCORE_TOP-padding; ++i)
	{
		count++;
		scoreBoard.append(stats.back());
		stats.pop_back();
		scoreBoard.append(",");
	}
	for (int i = 0; i < padding ; i++)
	{
		scoreBoard.append("null: 0");
		scoreBoard.append(",");
	}
	scoreBoard = scoreBoard.substr(0, scoreBoard.length() - 1);
	j["HighScore"] = scoreBoard;
		
	std::string userStats;
	for (int i = 0; i < STATS_NUM; ++i)
	{
		count++;
		userStats.append(stats.back());
		stats.pop_back();
		userStats.append(",");
	}
	userStats = userStats.substr(0, userStats.length() - 1);
	j["UserStatisics"] = userStats;
	
	std::string MsgS = j.dump();

	std::vector<unsigned char> MsgV;
	std::copy(MsgS.begin(), MsgS.end(), std::back_inserter(MsgV));

	int len = MsgS.length();
	std::vector <unsigned char>lenv = convertToVector(len);

	toSend.insert(toSend.end(), lenv.begin(), lenv.end());//adding lenth to vector

	toSend.insert(toSend.end(), MsgV.begin(), MsgV.end());//adding stats to vector

	return toSend;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeRespones(getPlayersInRoomRespones p)
{
	std::vector<unsigned char> toSend;
	toSend.push_back((unsigned char)GET_ROOM);

	nlohmann::json j;
	std::vector<std::string> players = p.players;
	std::string playersName;
	for (auto i = players.begin(); i != players.end(); ++i)
	{
		playersName.append(*i);
		playersName.append(",");
	}
	playersName = playersName.substr(0, playersName.length() - 1);
	j["PlayersInRoom"] = playersName;

	j["Name"] = p.name;
	j["MaxPlayers"] = p.maxPlayers;
	j["QuestionNum"] = p.questionNum;
	j["QuestionTime"] = p.questionTime;

	std::string MsgS = j.dump();

	std::vector<unsigned char> MsgV;
	std::copy(MsgS.begin(), MsgS.end(), std::back_inserter(MsgV));

	int len = MsgS.length();
	std::vector <unsigned char>lenv = convertToVector(len);

	toSend.insert(toSend.end(), lenv.begin(), lenv.end());//adding lenth to vector

	toSend.insert(toSend.end(), MsgV.begin(), MsgV.end());//adding players to vector

	return toSend;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeRespones(usersStateInGameResponse p)
{
	std::vector<unsigned char> toSend;
	toSend.push_back((unsigned char)GET_INGAME_STATES);

	nlohmann::json j;
	
	std::string userStringData;
	auto allData = p.allUsers;
	for (auto i = allData.begin(); i != allData.end(); ++i)
	{
		userStringData.append((*i).username); //0
		userStringData.append(",");
		userStringData.append(std::to_string((*i).x)); //1
		userStringData.append(",");
		userStringData.append(std::to_string((*i).y));//2
		userStringData.append(",");
		userStringData.append(std::to_string((*i).animationState));//3
		userStringData.append(",");
		userStringData.append(std::to_string((*i).action));//4
		userStringData.append(",");
		userStringData.append(std::to_string((*i).aliveState));//5
		userStringData.append(",");
		userStringData.append(std::to_string((*i).curUserRole));//6
		userStringData.append(",");
		userStringData.append(std::to_string((*i).isCurUser));//7
		userStringData.append(",");
		userStringData.append((*i).curAction);//8

		userStringData.append(";");

	}
	if (!userStringData.empty())
		userStringData.pop_back();
	j["PlayersInRoom"] = userStringData;
	
	std::string MsgS = j.dump();

	std::vector<unsigned char> MsgV;
	std::copy(MsgS.begin(), MsgS.end(), std::back_inserter(MsgV));

	int len = MsgS.length();
	std::vector <unsigned char>lenv = convertToVector(len);

	toSend.insert(toSend.end(), lenv.begin(), lenv.end());//adding lenth to vector

	toSend.insert(toSend.end(), MsgV.begin(), MsgV.end());//adding players to vector
	return toSend;
}

std::vector<unsigned char> JsonResponsePacketSerializer::convertToVector(int len)
{
	std::vector<unsigned char> lenv(INT_LEN);
	lenv[3] = (len >> 24) & 0xFF;
	lenv[2] = (len >> 16) & 0xFF;
	lenv[1] = (len >> 8) & 0xFF;
	lenv[0] = len & 0xFF;
	return lenv;
}

