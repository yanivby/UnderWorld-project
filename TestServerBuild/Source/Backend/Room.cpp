#include "../../Includes/Backend/Room.h"


Room::Room(RoomData metadata)
{
	this->metadata = metadata;
}

Room::~Room()
{
}

RoomData Room::getMetaData()
{
	return this->metadata;
}

std::string Room::getRoomName()
{
	return metadata.name;
}

void Room::addUser(loggedUser user)
{
	this->users.push_back(user);
	this->metadata.activeUsersCount++;
}

void Room::removeUser(loggedUser user)
{
	if (std::count(users.begin(), users.end(),user))
	{
		users.erase(std::find(users.begin(), users.end(), user));
		this->metadata.activeUsersCount--;
	}

}

std::vector<loggedUser> Room::getUsers()
{
	return users;
}
