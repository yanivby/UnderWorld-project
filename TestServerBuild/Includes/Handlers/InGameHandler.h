#pragma once
#include "IRequestHandler.h"
#include "../Managers/LoginManager.h"
#include "../Managers/RoomManager.h"
#include "../Managers/StatisticsManager.h"
#include "RequestHandlerFactory.h"
#include "../Backend/Room.h"

class RequestHandlerFactory;


class InGameHandler : public IRequestHandler
{
	RequestHandlerFactory* reqHandlerFactory;
	Room  curRoom;
	loggedUser curUser;
	

public:
	InGameHandler(RequestHandlerFactory* reqHandlerFactory, Room curRoom, loggedUser usr);

	virtual bool isRequestRelevent(RequestInfo r);
	virtual RequestResult handleRequest(RequestInfo r);
};
