#pragma once
#include "IRequestHandler.h"
#include "../../Includes/Managers/LoginManager.h"
#include "../../Includes/Managers/RoomManager.h"
#include "../../Includes/Managers/StatisticsManager.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;


class MenuRequestHandler : public IRequestHandler
{
	loggedUser user;
	RequestHandlerFactory* reqHandlerFactory;
	RequestResult signout(RequestInfo r);
	RequestResult getRooms(RequestInfo r);
	RequestResult getPlayersInRoom(RequestInfo r);
	//RequestResult getStatistics(RequestInfo r);
	RequestResult joinRoom(RequestInfo r);
	RequestResult createRoom(RequestInfo r);

public:
	MenuRequestHandler(loggedUser user, RequestHandlerFactory* reqHandlerFactory);
	enum returnCode {
		OK=1,
		EMPTY_ROOM_LIST,
		NO_PLAYERS_IN_ROOM,
		NO_STATISTICS,
		ROOM_DOESNT_EXIST,
		ROOM_FULL,
		ROOM_NAME_USED
	};
	virtual bool isRequestRelevent(RequestInfo r);
	virtual RequestResult handleRequest(RequestInfo r);
	loggedUser getUser();
};