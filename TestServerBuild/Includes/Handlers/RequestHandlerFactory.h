#pragma once
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "InGameHandler.h"
#include "../../Includes/Backend/IDatabase.h"
#include "../../Includes/Managers/LoginManager.h"
#include "../../Includes/Managers/RoomManager.h"
#include "../../Includes/Managers/InGameManager.h"
#include "../../Includes/Managers/StatisticsManager.h"

class LoginRequestHandler;
class MenuRequestHandler;
class InGameHandler;


class RequestHandlerFactory
{
private:
	IDatabase* database;
	loginManager *logMan;
	RoomManager* roomMan;
	InGameManager* InGameMan;
	StatisticsManager* statMan;

public:
	RequestHandlerFactory(IDatabase* db);
	~RequestHandlerFactory();
	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(loggedUser user);
	InGameHandler* createInGameRequestHandler(Room room, loggedUser usr);

	loginManager* getLoginManager();
	RoomManager* getRoomManager();
	InGameManager* getInGameManager();
	
	//StatisticsManager* getStatisticsManger();

};