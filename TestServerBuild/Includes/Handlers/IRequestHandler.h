#pragma once
#include <iostream>
#include <ctime>
#include <vector>

struct RequestInfo
{
	RequestInfo(int id, time_t recTime, std::vector<unsigned char> buf)
	{
		requestId = id;
		recivedTime = recTime;
		buffer = buf;
	}

	int requestId;
	time_t recivedTime;
	std::vector<unsigned char> buffer;
};

class IRequestHandler
{
public:
	struct RequestResult
	{
		std::vector<unsigned char> response;
		IRequestHandler* newHandler;
	};

	virtual bool isRequestRelevent(RequestInfo r) = 0;
	virtual RequestResult handleRequest(RequestInfo r) = 0;
};