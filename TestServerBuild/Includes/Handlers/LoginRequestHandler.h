#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
private:
	RequestHandlerFactory* handlerFactory;
public:
	LoginRequestHandler(RequestHandlerFactory* hf);

	virtual bool isRequestRelevent(RequestInfo r);
	virtual RequestResult handleRequest(RequestInfo r);
};