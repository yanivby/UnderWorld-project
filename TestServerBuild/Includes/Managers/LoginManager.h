#pragma once
#include "../../Includes/Backend/IDatabase.h"
#include "vector"
#include "string"

class loggedUser {
	std::string username;
public:
	loggedUser(std::string username);
	std::string getUserName();
	void setUsername(std::string username);
	bool operator==(const loggedUser other) ;
	inline bool operator< ( const loggedUser& rhs) const { return this->username < rhs.username; }

	};


class loginManager
{
	IDatabase* m_dataBase;
	std::vector <loggedUser> m_loggedUsers;

public:
	loginManager(IDatabase* m_dataBase);
	enum returnCode{
		OK,
		ALREADY_LOGGED_IN,
		INVALID_PASS,
		USER_DOES_NOT_EXIST,
		USERNAME_TAKEN,
		USERNAME_NOT_CONNECTED
	};
	returnCode signup(std::string username, std::string password, std::string email);
	returnCode login(std::string username, std::string password);
	returnCode logout(std::string username);
	bool isUserLogged(std::string username);

};