#pragma once
#include "LoginManager.h"
#include "RoomManager.h"
#include <map>
#define DEFAULT_START_X_VAL 50
#define DEFAULT_START_Y_VAL 50
#define SPAWN_POINT_COUNT 6
enum TASKS
{
	CATCH_THE_ORE,
	MINE_THE_ORE,
	SORT_THE_ORE,

};
enum Action
{
	MOVE,
	KILL,
	START_GAME,
	DO_TASK,

};


class vec2d
{
public:
	vec2d() {}
	vec2d(float x, float y);
	float x;
	float y;
};

class Task
{
public:
	Task(TASKS id) : taskId(id), Done(false)
	{
			
	}
	std::string descriptor;
	TASKS taskId;
	bool Done;
};

class PlayerState
{
public:
	PlayerState();
	PlayerState(std::vector<Task> tasks, int role);
	int alive;
	std::vector<Task> taskList;
	vec2d location;
	std::string curAction;
	int action;
	int curRole;
	int animationState;
	bool recentlyMoved;
};



class Game
{
public:

	int spawnPoints[SPAWN_POINT_COUNT][2] = { 
		{8 * 64, 12 * 64},
		{6 * 64, 59 * 64},
		{53 * 64, 58 * 64},
		{50 * 64, 3 * 64},
		{96 * 64, 52 * 64},
		{71 * 64, 56 * 64}};


	Room curRoom;
	bool gameStarted;
	std::map<loggedUser, PlayerState> playerStates;
	Game(): gameStarted(false){ }
	Game(Room curRoom);
	~Game() {}
	void UpdatePlayerState(loggedUser user, vec2d loc, int animation, int acc ,std::string curAction);
	std::map<loggedUser, PlayerState> getAllUsersData();
	void addUser(loggedUser usr);
	bool removeUser(loggedUser usr);
	void startGame();
	void checkWin();
	void minerWin();
	void impostorsWin();
	void DoTask(loggedUser usr, TASKS taskId);
	void killUser(loggedUser user);

};



class InGameManager
{
private:
	std::map <int,Game> games;
public:
	InGameManager();
	~InGameManager();

	void UpdatePlayerState(loggedUser user, vec2d loc, int animation, int acc, std::string curAction);
	std::map<loggedUser, PlayerState> getUsersInGame(loggedUser usr);
	void CreateNewGame(Room curRoom);
	bool joinGame(loggedUser usr, Room curRoom);
	bool leaveGame(loggedUser usr);
	int findGameWithUser(loggedUser usr);
	
private:

};

