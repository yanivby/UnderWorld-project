#pragma once
#include "../../Includes/Backend/Room.h"
#include "../../Includes/Backend/Serialization/JsonRequestPacketDeserializer.h"
#include <map>

class RoomManager
{
private:
	std::map<unsigned int, Room> rooms;
public:
	RoomManager(){}
	void deleteUser(loggedUser user);
	bool createRoom(loggedUser user, createRoomRequest data);
	void deleteRoom(unsigned int ID);
	unsigned int getRoomState(unsigned int ID);
	std::map<unsigned int, Room> getRooms();
	void addUser(int id, loggedUser user);
	int getRoomId(loggedUser usr);
	Room getRoomWithId(int id);
	Room FindRoomWithName(std::string roomName);
	void removeUser(int id, loggedUser user);
	void removeEmptyRooms();
};