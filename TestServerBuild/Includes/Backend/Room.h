#pragma once
#include "../../Includes/Managers/LoginManager.h"
#include <iostream>
#include <vector>

struct RoomData {
public:
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int isActive;
	unsigned int activeUsersCount;
};

class Room
{
private:
	RoomData metadata;
	std::vector<loggedUser> users;
public:
	Room(RoomData metadata);
	~Room();
	Room(){}

	bool operator==(Room rhs) {
		return this->metadata.name == rhs.metadata.name;
	}
	RoomData getMetaData();
	std::string getRoomName();
	void addUser(loggedUser user);
	void removeUser(loggedUser user);
	const std::string getRoomNameConst() {
		return metadata.name;
	}
	std::vector<loggedUser> getUsers();
};