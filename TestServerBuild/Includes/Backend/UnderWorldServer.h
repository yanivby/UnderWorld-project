#pragma once
#include <deque>
#include <queue>
#include <mutex>
#include <iostream>
#include <map>
#include <condition_variable>
#include <WinSock2.h>
#include "../../Includes/Handlers/LoginRequestHandler.h"
#include "sqliteDataBase.h"
#include "../../Includes/Handlers/RequestHandlerFactory.h"


class UnderWorldServer
{
public:
	UnderWorldServer();
	~UnderWorldServer();
	void serve();


private:
	void bindAndListen();
	void acceptClient();
	void clientHandler(SOCKET client_socket);
	std::map<SOCKET, LoginRequestHandler> users;
	SOCKET _socket;
	IDatabase* db;
	RequestHandlerFactory* factory;
};