#pragma once
#include <string>
#include <list>
#include "json.hpp"

/*class question
{
public:
	question(std::string questionText, std::string correct, std::string incorrect1, std::string incorrect2, std::string incorrect3): questionText(questionText), correct(correct),incorrect1(incorrect1),incorrect2(incorrect2),incorrect3(incorrect3){}
	std::string questionText;
	std::string correct;
	std::string incorrect1;
	std::string incorrect2;
	std::string incorrect3;
};*/
class IDatabase
{
public:
	virtual bool doesUserExist(std::string username) = 0;
	virtual bool doesPasswordMatch (std::string username , std::string password) = 0;
	virtual bool addNewUser(std::string username, std::string password, std::string email) = 0; 
	//virtual std::list<question> getQuestions(int) = 0;
	//virtual float PlayerAvarageAnswerTime(std::string) = 0;
	//virtual int getNumOfCorrectAnswers(std::string) = 0;
	//virtual int getNumOfTotalAnswers(std::string) = 0;
	//virtual int getNumOfPlayerGames(std::string) = 0;
	//virtual void loadQuestionsFromJson(nlohmann::json questions) = 0;
	virtual std::vector<std::string> getAllUsers() = 0;
};