#pragma once
#include "IDatabase.h"
#include "sqlite3.h"
#define DB_PATH "DB.db"
/*
class statistics {
public:
	statistics(float avgAnswerTime, int correctAnswerCount, int totalAnswerCount, int playerGameCount) : avgAnswerTime(avgAnswerTime), correctAnswerCount(correctAnswerCount), totalAnswerCount(totalAnswerCount), playerGameCount(playerGameCount) {}
	float avgAnswerTime;
	int correctAnswerCount;
	int totalAnswerCount;
	int playerGameCount;
};*/
class sqliteDataBase : public IDatabase
{
public:
	sqliteDataBase();
	~sqliteDataBase();
	virtual bool doesUserExist(std::string username);
	virtual bool doesPasswordMatch(std::string username, std::string password);
	virtual bool addNewUser(std::string username, std::string password, std::string email);
	//virtual std::list<question> getQuestions(int numOfQuestions);
	//virtual float PlayerAvarageAnswerTime(std::string username);
	//virtual int getNumOfCorrectAnswers(std::string username);
	//virtual int getNumOfTotalAnswers(std::string username);
	//virtual int getNumOfPlayerGames(std::string username);
	//virtual void loadQuestionsFromJson(nlohmann::json questions);
	virtual std::vector<std::string> getAllUsers();
	nlohmann::json getJsonFromFile(std::string fileName);


private:
	sqlite3 *DB;
	//statistics* getStatistics(std::string username);
};
int callbackUserExists(void* data, int argc, char** argv, char** azColName);
//int callbackQuestions(void* data, int argc, char** argv, char** azColName);
//int callbackStatistics(void* data, int argc, char** argv, char** azColName);
int callbackUser(void* data, int argc, char** argv, char** azColName);