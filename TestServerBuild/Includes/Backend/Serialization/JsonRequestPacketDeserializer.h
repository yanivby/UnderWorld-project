#pragma once
#include "../json.hpp"
#include <string>
#include <vector>
#include <iostream>

enum CODES {
	LOGIN_CODE = 100,
	SIGNUP_CODE,
	LOGOUT_CODE,
	JOIN_ROOM_CODE,
	CREATE_ROOM_CODE,
	GET_PLAYERS_CODE,
	GET_STATS_CODE,
	GET_ROOM_CODE,
	UPDATE_INGAME_STATE
};

struct loginRequest {
	std::string userName;
	std::string password;
};

struct signupRequset {
	std::string userName;
	std::string password;
	std::string email;
};

struct getPlayerInRoomRequest
{
	unsigned int roomId;
};

struct joinRoomRequest
{
	unsigned int roomId;
};

struct createRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	
};


struct UpdatePlayerStateRequest {
	float x;
	float y;
	int animation;
	int action;
	std::string curAction;
};



struct JsonRequestPacketDeserializer
{
	static loginRequest deserializeLoginRequest(std::vector<unsigned char> buffer);
	static signupRequset deserializeSignupRequest(std::vector<unsigned char> buffer);
	static getPlayerInRoomRequest desrializeGetInRoomRequest(std::vector<unsigned char> buffer);
	static joinRoomRequest desrializeJoinRoomRequest(std::vector<unsigned char> buffer);
	static createRoomRequest desrializeCreateRoomRequest(std::vector<unsigned char> buffer);
	static UpdatePlayerStateRequest desrializeUpdatePlayerStateRequest(std::vector<unsigned char> buffer);

	static nlohmann::json getJson(std::vector<unsigned char> buffer);
	static int buffToInteger(std::vector<unsigned char> buffer); //turn 4 bytes to int
private:
	
};
