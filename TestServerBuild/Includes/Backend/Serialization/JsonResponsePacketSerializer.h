#pragma once
#include "../Room.h"
#include "../../Managers/InGameManager.h"
#include <iostream>
#include <vector>

struct baseRespones
{
	unsigned int status;
};

struct loginRespones : public baseRespones
{
	
};

struct singupRespones : public baseRespones
{

};

struct logoutRespones : public baseRespones
{

};

struct joinRoomRespones : public baseRespones
{

};

struct createRoomRespones : public baseRespones
{

};

struct getRoomsRespones : public baseRespones
{
	std::vector<RoomData> rooms;
};

struct getStatisticsRespones : public baseRespones
{
	std::vector<std::string> statistics;
};

struct getPlayersInRoomRespones
{
	std::vector<std::string> players;
	int questionTime;
	int questionNum;
	int maxPlayers;
	std::string name;
};



struct userData {
	float x;
	float y;
	int animationState;
	int aliveState;
	int curUserRole;
	bool isCurUser;
	int action;
	std::string curAction;
	std::string username;
	

};
struct usersStateInGameResponse
{
	std::vector<userData> allUsers;
};

struct errorRespones
{
	std::string errorMsg;
};


class JsonResponsePacketSerializer
{
public:
	/*
	function will make an error msg to send to server
	input - errorRespone
	output - msg to send to client
	*/
	static std::vector<unsigned char> serializeRespones(errorRespones e);
	/*
	function will make a login respones to client
	input - loginrespones
	output - msg to send to client
	*/
	static std::vector<unsigned char> serializeRespones(loginRespones l);
	/*
	function will make a sing up respones to client
	input - sunguprespone
	output - msg to send to client
	*/
	static std::vector<unsigned char> serializeRespones(singupRespones s);
	static std::vector<unsigned char> serializeRespones(logoutRespones l);
	static std::vector<unsigned char> serializeRespones(joinRoomRespones s);
	static std::vector<unsigned char> serializeRespones(createRoomRespones c);
	static std::vector<unsigned char> serializeRespones(getRoomsRespones r);
	static std::vector<unsigned char> serializeRespones(getStatisticsRespones s);
	static std::vector<unsigned char> serializeRespones(getPlayersInRoomRespones p);
	static std::vector<unsigned char> serializeRespones(usersStateInGameResponse p);


	static std::vector<unsigned char> convertToVector(int len);

};