import socket, threading
import json

class gameData():
    def __init__(self):
        self.userList = {}
    def set_pos(self, userId, data):
        self.userList[userId] = data 
    def get_pos(self, userId):
        return  self.userList[userId]
    def remove_from_List(self,userId):
        try:
            self.userList.pop(userId)
        except:
            pass
    def get_all_users_data(self, userId):
        allUsers = []
        for each in self.userList.items():
            if (each[0] != userId):
                x = dict()
                x["x"] = str(each[1][0])
                x["y"] = str(each[1][1])
                x["animation"] = str(each[1][2])
                #print(x["animation"])
                x["userId"] = str(each[0])
                allUsers.append (x) 
        return allUsers

class ClientThread(threading.Thread):
    def __init__(self,clientAddress,clientsocket, game_data, id):
        threading.Thread.__init__(self)
        self.csocket = clientsocket
        self.game_data = game_data
        self.userId = id
        print ("New connection added: ", clientAddress)
    
    def run(self):
        print ("Connection from : ", clientAddress)
        #self.csocket.send(bytes("Hi, This is from Server..",'utf-8'))
        msg = ''
        while True:
            try:
                data = self.csocket.recv(512)
                msg = data.decode()
                if msg=='bye':
                    break
                #print ("from client", msg)
                self.csocket.send(bytes(self.processData(msg),'UTF-8'))
            except :
                #print ("Client at ", clientAddress , " disconnected...")
                game_data.remove_from_List(self.userId)
                break
        print ("Client at ", clientAddress , " disconnected...")
        game_data.remove_from_List(self.userId)
    
    def processData(self, data):
        if data != 'null':
            from_client = json.loads(data)
            loc = (from_client["x"],from_client["y"],from_client["animation"],)
            self.game_data.set_pos(self.userId, loc)
            return json.dumps(game_data.get_all_users_data(self.userId))
        else: return "no data"



LOCALHOST = "192.168.1.176"
PORT = 8826
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server.bind((LOCALHOST, PORT))
print("Server started")
print("Waiting for client request..")
game_data = gameData()
firstUserId = 0
server.listen(1)
while True:
    firstUserId += 1
    clientsock, clientAddress = server.accept()
    newthread = ClientThread(clientAddress, clientsock, game_data, firstUserId)
    newthread.start()