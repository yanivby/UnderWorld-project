﻿using UnderWorld.DrawableObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using UnderWorld.Backend;



namespace UnderWorld.Managers
{
    public enum INTERACTABLE_ID
    {
        CATCHTHEORE,
        MINETHEORE,
        SORTTHEORE,
        NONE,

    }

    class InteractionObject
    {
        
        public InteractionObject(INTERACTABLE_ID id, Vector2 location = new Vector2())
        {
            this.id = id;
            this.location = location;

            switch (id)
            {
                case INTERACTABLE_ID.NONE:
                    break;
                case INTERACTABLE_ID.MINETHEORE:
                    break;
                case INTERACTABLE_ID.CATCHTHEORE:
                    break;
                case INTERACTABLE_ID.SORTTHEORE:
                    break;
                default:
                    break;
            }
        }
        public Vector2 location;
        public INTERACTABLE_ID id;
        public bool canUse = true;
    }

    class InteractableManager
    {
        const float MAX_DISTANCE = 300;
        public List<InteractionObject> _Interactabls;
        public InteractableManager(List<InteractionObject> list)
        {
            _Interactabls = list;
        }

        public InteractableManager()
        {
            _Interactabls = new List<InteractionObject>();
        }
        public void addInteractable(InteractionObject obj)
        {
            _Interactabls.Add(obj);
        }

        public INTERACTABLE_ID getInRange(Vector2 rect)
        {
            float closest = float.MaxValue;
            INTERACTABLE_ID closestId = INTERACTABLE_ID.NONE;
            foreach (var each in _Interactabls)
            {
                if (each.canUse && distance(rect.X, rect.Y, each.location.X, each.location.Y) < MAX_DISTANCE && distance(rect.X, rect.Y, each.location.X, each.location.Y) < closest)
                {
                    closest = distance(rect.X, rect.Y, each.location.X, each.location.Y);
                    closestId = each.id;
                }

            }
            return closestId;

        }


        public void setAsDone (INTERACTABLE_ID id)
        {

            Communicator.FinishTask(id);
            foreach (var each in _Interactabls)
            {
                if (each.id == id)
                {
                    each.canUse = false;
                }
            }

        }




        public static float distance(float x1, float y1, float x2, float y2)
        {
            return (float)Math.Sqrt(((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
        }
    }


}

