﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Penumbra;
using System;
using System.Collections.Generic;
using System.Text;
using UnderWorld.Backend;

namespace UnderWorld.Managers
{
    class MiniGameManager : StateManager
    {
        public enum States
        {
            CATCH_THE_ORE,
            MINE_ORE,
            PROCESS_ORE,
            PLANT_DYNAMITE,
            SUBMIT_ORE

        }

        public MiniGameManager(SpriteBatch spriteBatch, Game game, GraphicsDeviceManager graphics, Communicator comms, PenumbraComponent penu): 
        base(spriteBatch, game, graphics, comms, penu)
        {

            _CurrentState = null;

        }

        public void ChangeState(States state)
        {
            _lastState = _CurrentState;

            switch (state)
            {
                case States.CATCH_THE_ORE:
                    break;
                case States.MINE_ORE:
                    break;
                case States.PROCESS_ORE:
                    break;
                case States.PLANT_DYNAMITE:
                    break;
                case States.SUBMIT_ORE:
                    break;
            }
        }
    }
}
