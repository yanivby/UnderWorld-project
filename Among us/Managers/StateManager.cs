﻿using UnderWorld.GameStates;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using UnderWorld.Backend;
using Penumbra;
using UnderWorld.MiniGame;

namespace UnderWorld.Managers
{
    public enum States
    {
        MAIN_MENU,
        ROOM_MANAGER,
        IN_ROOM,
        INGAME,
        PAUSE,
        LOGIN,
        SIGNUP,
        OPTIONS,
        STARTUP,
        CREATE_ROOM,
        CATCH_THE_ORE,
        MINE_ORE,
        SORT_ORES,
        MANUAL
        

    }
     
    public class StateManager 
    {
        public State _CurrentState;
        public State _lastState;
        public SpriteBatch _spriteBatch;
        public GraphicsDeviceManager _graphics;
        public Game _Game;
        public Communicator _comms;
        public bool _fullScreen;
        public PenumbraComponent _penumbra;


        public StateManager(SpriteBatch spriteBatch, Game game, GraphicsDeviceManager  graphics, Communicator comms, PenumbraComponent penu )
        {
            _spriteBatch = spriteBatch;
            _Game = game;
            _graphics = graphics;
            _comms = comms;
            _CurrentState = new Manual(this);
            _fullScreen = false;
            _penumbra = penu;
            _penumbra.Visible = true;

        }

        public void Update(GameTime gameTime)
        {
            if(_CurrentState != null)
            {
                _CurrentState.update(gameTime);

            }
        }

        public void Draw (GameTime gameTime)
        {
            if (_CurrentState != null)
            {
                _CurrentState.Draw(gameTime);
            }
        }


        public void DrawAbove ( GameTime gameTime)
        {
            if (_CurrentState != null)
            {
                _CurrentState.Draw(gameTime);
            }
        }
        public void LastState()
        {
            if (_lastState != null)
            {
                _CurrentState = _lastState;
                _CurrentState.Load();
                _CurrentState.reload();
                _lastState = null;
            }
        }

        public void ChangeState (States state  )
        {
            _lastState = _CurrentState;
            _penumbra.Visible = false;

            switch (state)
            {
                case States.MAIN_MENU:
                    _CurrentState = new MainMenu(this);
                    break;
                case States.ROOM_MANAGER:
                    _CurrentState = new RoomMenu(this);
                    break;
                case States.INGAME:
                    _CurrentState = new InGame(this);
                    break;
                case States.PAUSE:
                    _CurrentState = new PauseMenu(this);
                    break;
                case States.LOGIN:
                    _CurrentState = new loginMenu(this);
                    break;
                case States.SIGNUP:
                    _CurrentState = new SignupMenu(this);
                    break;
                case States.OPTIONS:
                    _CurrentState = new OptionsMenu(this);
                    break;
                case States.STARTUP:
                    _CurrentState = new StartUpMenu(this);
                    break;
                case States.CREATE_ROOM:
                    _CurrentState = new CreateRoom(this);
                    break;
                case States.CATCH_THE_ORE:
                    _CurrentState = new CatchTheOre(this);
                    break;
                case States.MINE_ORE:
                    _CurrentState = new Mine(this);
                    break;
                case States.SORT_ORES:
                    _CurrentState = new SortTheOre(this);
                    break;
                case States.MANUAL:
                    _CurrentState = new Manual(this);

                    break;
                default:
                    break;
            }
        }


    }
}
