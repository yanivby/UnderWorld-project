﻿using UnderWorld.Backend;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnderWorld.Managers
{
    public class AnimationManager
    {
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_animation.Texture,
                new Rectangle(Convert.ToInt32(Position.X),
                              Convert.ToInt32(Position.Y),
                (int)((_animation.FrameWidth / 100.0) * scale),
                (int)((_animation.FrameHeight / 100.0) * scale)),
                
                new Rectangle(_animation.CurrentFrame * _animation.FrameWidth,
                0,
                _animation.FrameWidth ,
                _animation.FrameHeight ),
                _animation.Color); ;
        }
        public void setColor(Color c)
        {
            _animation.Color = c;
        
        }
        public Rectangle _rectangle
        {
            get
            {
                return new Rectangle(Convert.ToInt32(Position.X),
                Convert.ToInt32(Position.Y),
                (int)((_animation.FrameWidth / 100.0) * scale),
                (int)((_animation.FrameHeight / 100.0) * scale));
            }
            set {; }
        }

        private Animation _animation;
        private float _timer;
        public Vector2 Position { get; set; }
        public int scale { get; set; }

        public float AnimationHeight { get { if (_animation == null) return 0; else return _animation.FrameHeight; } }
        public float AnimationWidth { get { if (_animation == null) return 0; else return _animation.FrameWidth; } }

        public AnimationManager(Animation animation)
        {
            _animation = animation;
        }
        public void Play(Animation animation)
        {
            if (_animation == animation)
                return;
            _animation = animation;
            _animation.CurrentFrame = 0;
            _timer = 0;
        }

        public void Stop()
        {
            _timer = 0f;
            _animation.CurrentFrame = 0;
        }
        public void Update(GameTime gameTime)
        {
            _timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (_timer > _animation.FrameSpeed)
            {
                _timer = 0f;
                _animation.CurrentFrame++;
                if (_animation.CurrentFrame >= _animation.FrameCount)
                    _animation.CurrentFrame = 0;
            }
        }


    }
}
