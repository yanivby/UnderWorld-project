﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Collections.Specialized;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using UnderWorld.Backend;
using Penumbra;
using Microsoft.Xna.Framework.Graphics;

namespace UnderWorld.Managers
{
    class LightingManager
    {
        Dictionary<string, Light> StaticLights = new Dictionary<string, Light>();
        OrderedDictionary GlobalLights = new OrderedDictionary();
        OrderedDictionary GlobalHulls = new OrderedDictionary();
        Dictionary<string, Hull> StaticHulls = new Dictionary<string, Hull>();

        public bool enabled = false;

        public PenumbraComponent _penumbra;
        public LightingManager(ref PenumbraComponent _penumbra)
        {
            this._penumbra = _penumbra;
        }
        
        public void addLightStatic(Light light, string id )
        {
            try
            {
                StaticLights.Add(id, light);
                _penumbra.Lights.Add(light);
                remake();
            }
            catch
            {

            }

        }
        public void addHullStatic(Hull light, string id)
        {
            try
            {
                StaticHulls.Add(id, light);
                _penumbra.Hulls.Add(light);
                remake();
            }
            catch
            {

            }

        }
        public void addLightGlobal(Light light, string id )
        {
            try
            {
                GlobalLights.Add(id, light);
                _penumbra.Lights.Add(light);
                remake();
            }
            catch(Exception e)
            {

            }
            
            
        }
        public void removeLightGlobal(string id)
        {
            GlobalLights.Remove(id);
            remake();
        }

        public void addHullGlobal(Hull hull, string id)
        {
            try
            {
                GlobalHulls.Add(id, hull);
                _penumbra.Hulls.Add(hull);
                remake();
            }
            catch
            {

            }
        }
        public void removeHullGlobal(string id)
        {
            GlobalHulls.Remove(id);
            remake();
        }


        public void remake()
        {
            _penumbra.Hulls.Clear();
            _penumbra.Lights.Clear();

            if (_penumbra != null && enabled)
            {
                foreach (var lamp in GlobalLights)
                {
                    Light NewCopy = new PointLight()
                    {
                        Enabled = true,
                        Position = new Vector2( ),
                        Scale = new Vector2(600.0f),
                        ShadowType = ShadowType.Solid,
                        Color = Color.Pink,
                        Intensity = 0,

                    };
                    _penumbra.Lights.Add(NewCopy);
                }
                foreach (var hul in GlobalHulls)
                {
                    var NewCopy = new Hull(new Vector2(1.0f), new Vector2(-1.0f, 1.0f), new Vector2(-1.0f), new Vector2(1.0f, -1.0f))
                    {
                        

                    };
                    _penumbra.Hulls.Add(NewCopy);

                }
                foreach (KeyValuePair<string, Light> each in StaticLights)
                {
                    _penumbra.Lights.Add(each.Value);
                }
                foreach (KeyValuePair<string, Hull> each in StaticHulls)
                {
                    _penumbra.Hulls.Add(each.Value);
                }
            }
        }


        public void update(GameTime gameTime, Vector2 location)
        {
            if (_penumbra != null && enabled)
            {
                int count = 0;

                foreach (var lamp in _penumbra.Lights)
                {

                    if (count < GlobalLights.Count)
                    {
                        var each = ((Light)GlobalLights[count]);
                        lamp.Position = new Vector2(each.Position.X - location.X, each.Position.Y - location.Y);
                        lamp.Scale = new Vector2(600.0f);
                        lamp.ShadowType = ShadowType.Solid;
                        lamp.Color = Color.Pink;
                        lamp.Intensity = each.Intensity;
                        count++;
                    }
                 }
                
                
                count = 0;
                foreach (var hul in _penumbra.Hulls)
                {
                    if ( count < GlobalHulls.Count)
                    {
                        var each = ((Hull)GlobalHulls[count]);
                        hul.Position = new Vector2(each.Position.X - location.X, each.Position.Y - location.Y);
                        hul.Scale = each.Scale;
                        hul.Enabled = true;
                        count++;
                    }
                   
                }

                
             
            }
        }
        



    }
}
