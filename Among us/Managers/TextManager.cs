﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;


namespace UnderWorld.Managers
{

    public class TextObject 
    {
        public Vector2 _Position;
        public string _TextValue;
        public string _FieldName = "";
        public Color _Color;
    }

    public class TextManager
    {
        private SpriteFont _MainFont;
        private List<TextObject> _TextObjects;
        private int _height = 20;
        public TextManager(SpriteFont spriteFont, List<TextObject> textLists)
        {
            _MainFont = spriteFont;
            _TextObjects = textLists;
        }

        public TextManager(SpriteFont spriteFont)
        {
            _MainFont = spriteFont;
            _TextObjects = new List<TextObject>();
        }

        public void AddTextObject(string text, Vector2 location, string fieldName)
        {
            AddTextObject(text, location, fieldName, Color.White);
        }
        public void AddTextObject(string text, Vector2 location, string fieldName, Color color)
        {
            _TextObjects.Add(new TextObject()
            {
                _Position = location,
                _FieldName = fieldName,
                _TextValue = text,
                _Color = color,
            });
        }

        public float getFontHeight()
        {
            return _height;
        }

        public void RemoveAll()
        {
            _TextObjects.RemoveAll(item =>
            {
                return true;
            });
        }

        public void RemoveTextObject(string FieldName)
        {
            _TextObjects.RemoveAll(item =>
            {
                return item._FieldName == FieldName;
            });
        }
        public void UpdateSingle(string FieldName, string NewData)
        {
            foreach (var each in _TextObjects)
            {
                if (each._FieldName == FieldName)
                    each._TextValue = NewData;
            }
        }
        public void UpdateSingle(string FieldName, Vector2 position)
        {
            foreach (var each in _TextObjects)
            {
                if (each._FieldName == FieldName)
                {
                    each._Position = position;
                }

            }
        }
        public string getValue(string FieldName)
        {
            foreach (var each in _TextObjects)
            {
                if (each._FieldName == FieldName)
                {
                    return each._TextValue;
                }

            }
            return "error";

        }

        public void UpdateSingle(string FieldName, string NewData, Vector2 position)
        {
            foreach (var each in _TextObjects)
            {
                if (each._FieldName == FieldName)
                {
                    each._TextValue = NewData;
                    each._Position = position;
                }

            }
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            foreach (var each in _TextObjects)
            {
                spriteBatch.DrawString(_MainFont, each._TextValue, each._Position, each._Color);

            }
        }
    }
}
