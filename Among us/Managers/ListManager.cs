﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using UnderWorld.Backend;
using UnderWorld.Managers;
using UnderWorld.Controls;
using System.Security.Cryptography.X509Certificates;
using System.Linq;

namespace UnderWorld.Managers
{

    class ListManager 
    {
        private List<Component> _buttons;
        private Vector2 _location;
        private TextManager _textman { get; set; }
        private List<string> _contentList;
        SpriteFont spriteFont;
        Texture2D buttonTexture;
        public string sellectedId;
        public event EventHandler sellectionMade;


        public List<string> ContentList { get { return _contentList; } set { updateContent(); _contentList = value; } }
        public ListManager(Texture2D buttonTexture, SpriteFont spriteFont, Vector2 loc)
        {
            this.buttonTexture = buttonTexture;
            this.spriteFont = spriteFont;
            _textman = new TextManager(spriteFont);
            _location = loc;

        }



        private void updateContent()
        {
            _textman.RemoveAll();
            _buttons = new List<Component>();
            load();
        }
        public void load()
        {
            int height = 0;
            int id = 0;
            if (_contentList != null)
            {
                foreach (var each in _contentList)
                {
                    if (each == "")
                        continue;


                    _textman.AddTextObject(each,
                        new Vector2(_location.X, _location.Y+ height),
                        id + "");
                    

                    Button temp = new Button(buttonTexture, spriteFont)
                    {
                        Position = new Vector2(_location.X + 500, _location.Y + Convert.ToInt32(_textman.getFontHeight()*2) + height),
                        text = "Join",
                        PenColor = Color.Black,
                        id = id + "",
                        scale = 3,


                    };
                    id++;
                    height += Convert.ToInt32(_textman.getFontHeight())+10;

                    temp.click += click;
                    _buttons.Add(temp);
                }
            }
        }

        private void click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            sellectedId = _textman.getValue(btn.id);
            sellectionMade?.Invoke(this, new EventArgs());
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            _textman.Draw(spriteBatch);
            foreach (var each in _buttons)
            {
                each.Draw(spriteBatch);
            }
        }

        public void update(GameTime gameTime)
        {
            foreach (var each in _buttons)
            {
                each.Update(gameTime);
            }
        }
        
    }
}
