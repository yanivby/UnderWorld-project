﻿using UnderWorld.Backend;
using UnderWorld.Managers;
using UnderWorld.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using Penumbra;

namespace UnderWorld.GameStates
{
    class PauseMenu : State
    {
        private List<Component> buttons;

        public PauseMenu(StateManager man) : base(man)
        {
            Load();
        }

        public override void Load()
        {
            Button StartGame = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 - 60),
                text = "Resume",
                PenColor = Color.Black,
            };

            Button BackToMain = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2+20),
                text = "Back To Menu",
                PenColor = Color.Black,
            };
            Button fullscreen = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                      _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 + 100),
                text = "FullScreen",
                PenColor = Color.Black,
            };


            StartGame.click += StartGame_click;
            BackToMain.click += BackToMain_click;
            fullscreen.click += Fullscreen_click;

            buttons = new List<Component>()
            {
                StartGame,
                BackToMain,

            };
        }

        private void Fullscreen_click(object sender, EventArgs e)
        {
            _StateManager._fullScreen = !_StateManager._fullScreen;

            if (_StateManager._fullScreen)
            {
                _graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
                _graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
                _graphics.ApplyChanges();
                _StateManager._graphics.ToggleFullScreen();

            }
            else
            {
                _graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width / 2;
                _graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height / 2;
                _graphics.ApplyChanges();
                _StateManager._graphics.ToggleFullScreen();

            }
            //_StateManager._penumbra = new PenumbraComponent(this._game);
            //_StateManager._Game.Components.Clear();
            //_StateManager._Game.Components.Add(_StateManager._penumbra);
            //_StateManager._penumbra.Visible = true;
            _StateManager._penumbra.Initialize();
            this._StateManager.ChangeState(States.PAUSE);


        }
        private void BackToMain_click(object sender, EventArgs e)
        {
            Communicator.hasPreviousLocation = false;
            Communicator.ingame = false;
            while(Communicator.thread.IsAlive)
            {

            }
            Communicator.LeaveRoom();
            this._StateManager.ChangeState(States.MAIN_MENU);

        }

        private void StartGame_click(object sender, EventArgs e)
        {
            this._StateManager.ChangeState(States.INGAME);
        }

        public override void update(GameTime gameTime)
        {
            foreach (var button in buttons)
            {
                button.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();

            foreach (var button in buttons)
                button.Draw(_spriteBatch);

            _spriteBatch.End();

        }
    }
}
