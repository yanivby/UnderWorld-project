﻿using UnderWorld.Backend;
using UnderWorld.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnderWorld.GameStates
{
    public abstract class State
    {
        public StateManager _StateManager;
        public SpriteBatch _spriteBatch;
        public GraphicsDeviceManager _graphics;
        public Game _game;
        public Communicator _comms;
        public State(StateManager man)
        {
            _StateManager = man;
            _spriteBatch = man._spriteBatch;
            _graphics = man._graphics;
            _game = man._Game;
            _comms = man._comms;
        }

        public abstract void Load();
        public abstract void update(GameTime gameTime);
        public abstract void Draw(GameTime gameTime);
        public virtual void DrawAbove(GameTime gameTime) { 
        
        }
        public virtual void reload()
        {

        }


    }
}
