﻿using UnderWorld.Managers;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using UnderWorld.Controls;

namespace UnderWorld.GameStates
{
    public class Manual : State
    {
        private TextManager _textManager;
        private Button continueButton;
        private string ManualString =   "Welcome To UNDERWORLD!\n" +
                                        "in this game, there are two roles -\n" +
                                        "a murderer and a miner\n" +
                                        "the murderer's task is to find all of the miners and kill them before \n" +
                                        "they finish their tasks\n" +
                                        "the miner's task is to find all the tasks in the map and complete them\n" +
                                        "each player can do up to 3 tasks - once 2/3 out of all the tasks are \n" +
                                        "done - the miners win!\n" +
                                        "if the murderer kills them before they finish their \n" +
                                        "tasks - the murderer wins!\n" +
                                        "once you die as a miner your role is not yet over. you must \n" +
                                        "continue doing your tasks as a ghost!\n" +
                                        "have fun! ";

        public Manual(StateManager man) : base(man)
        {
            Load();
        }

        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();

            _textManager.Draw(_spriteBatch);
            continueButton.Draw(_spriteBatch);

            _spriteBatch.End();
        }

        public override void Load()
        {
            _textManager = new TextManager(this._game.Content.Load<SpriteFont>("Fonts/MainFont"));
            _textManager.AddTextObject(ManualString, new Vector2(20, 50), "manual");
            continueButton = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                           _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight - 50),
                text = "Continue",
                PenColor = Color.Black,
            };
            continueButton.click += ContinueButton_click;
        }

        private void ContinueButton_click(object sender, EventArgs e)
        {
            _StateManager.ChangeState(States.STARTUP);
        }

        public override void update(GameTime gameTime)
        {
            continueButton.Update(gameTime);
        }
    }
}