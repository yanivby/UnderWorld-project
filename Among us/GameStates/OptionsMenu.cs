﻿using UnderWorld.Backend;
using UnderWorld.Managers;
using UnderWorld.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnderWorld.GameStates
{
    class OptionsMenu : State
    {
        private List<Component> buttons;

        public OptionsMenu(StateManager man) : base(man)
        {
            Load();
        }

        public override void Load()
        {
            Button fullscreen = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 - 60),
                text = "FullScreen",
                PenColor = Color.Black,
            };

            Button Back = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 + 20),
                text = "Back",
                PenColor = Color.Black,
            };



            Back.click += Back_click; ;
            fullscreen.click += Fullscreen_click;

            buttons = new List<Component>()
            {
                Back,
                fullscreen

            };
        }

        private void Back_click(object sender, EventArgs e)
        {
            _StateManager.LastState();
        }

        private void Fullscreen_click(object sender, EventArgs e)
        {
            _StateManager._fullScreen = !_StateManager._fullScreen;
            if (_StateManager._fullScreen)
            {
                _graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
                _graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
                _graphics.ApplyChanges();
            }
            else
            {
                _graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width / 2;
                _graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height / 2;
                _graphics.ApplyChanges();
            }
            Load();

        }
       

        public override void update(GameTime gameTime)
        {
            foreach (var button in buttons)
            {
                button.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();

            foreach (var button in buttons)
                button.Draw(_spriteBatch);

            _spriteBatch.End();

        }
    }
}
