﻿using UnderWorld.Backend;
using UnderWorld.DrawableObjects;
using UnderWorld.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;
using UnderWorld.Controls;


namespace UnderWorld.GameStates
{
    public class RoomMenu : State
    {
        private ListManager _listman;
        private TextManager _textMan;
        private List<Component>components;
        public RoomMenu(StateManager man) : base(man)
        {
            _listman = new ListManager(this._game.Content.Load<Texture2D>("Controls/Button"),
                          _game.Content.Load<SpriteFont>("Fonts/ButtonFont"), new Vector2(250, 20));
            _textMan = new TextManager(_game.Content.Load<SpriteFont>("Fonts/ButtonFont"));
            
            _listman.ContentList = new List<string>(Communicator.getrooms().Item2);
            _listman.sellectionMade += Listman_sellectionMade;
            _textMan.AddTextObject("", new Vector2(this._graphics.PreferredBackBufferWidth / 2 - 200, this._graphics.PreferredBackBufferHeight / 2 + 200), "error");


            Button refresh = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2 +220, this._graphics.PreferredBackBufferHeight / 2 + 150),
                text = "refresh",
                PenColor = Color.Black,
            };
            Button backToMain = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2 -220, this._graphics.PreferredBackBufferHeight / 2 + 150),
                text = "Back",
                PenColor = Color.Black,
            };
            Button createRoom = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 + 150),
                text = "Create Room",
                PenColor = Color.Black,
            };


            refresh.click += Refresh_click;
            backToMain.click += BackToMain_click;
            createRoom.click += CreateRoom_click;

            components = new List<Component>()
            {
                refresh,
                backToMain,
                createRoom,

            };
            Refresh_click(null, null);
        }

        private void CreateRoom_click(object sender, EventArgs e)
        {
            _StateManager.ChangeState(States.CREATE_ROOM);
        }

        private void Listman_sellectionMade(object sender, EventArgs e)
        {
            ListManager temp = (ListManager)sender;
            string res = Communicator.joinRoom(new JoinRoomRequest(temp.sellectedId.Split(" - ")[0]));
            if (res =="1")
            {
                _StateManager.ChangeState(States.INGAME);

            }
            else
            {
                _textMan.UpdateSingle("error", res);
            }
        }
        private void Refresh_click(object sender, EventArgs e)
        {
            _listman.ContentList = new List<string>(Communicator.getrooms().Item2);
        }
        private void BackToMain_click(object sender, EventArgs e)
        {
            _StateManager.ChangeState(States.MAIN_MENU);
        }

        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();

            _textMan.Draw(this._spriteBatch);
            _listman.Draw(this._spriteBatch);
            foreach (var component in components)
            {
                component.Draw(_spriteBatch);
            }
            _spriteBatch.End();

        }

        public override void Load()
        {
            throw new NotImplementedException();
        }

        public override void update(GameTime gameTime)
        {
            _listman.update(gameTime);
            foreach (var component in components)
            {
                component.Update(gameTime);
            }
        }
    }
}
