﻿using UnderWorld.Backend;
using UnderWorld.Controls;
using UnderWorld.Managers;
using Microsoft.VisualBasic.CompilerServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnderWorld.GameStates
{
    public class MainMenu : State
    {
        private List<Component> buttons = new List<Component>();
        private Camera _camera;

        public MainMenu(StateManager man) : base(man)
        {
            Load();

        }

        public override void Load()
        {        
            Button findGame = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            { 
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 - 60 ),
                text = "Find Game",
                PenColor = Color.Black,
            };

            Button exitGame = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 + 20),
                text = "Quit",
                PenColor = Color.Black,
            };
            Button fullscreen = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 + 100),
                text = "FullScreen",
                PenColor = Color.Black,
            };
            findGame.click += FindGame_click; ;
            exitGame.click += ExitGame_click;
            fullscreen.click += Fullscreen_click;
            buttons = new List<Component>()
            {
                findGame,
                exitGame,
            };
        }

        private void FindGame_click(object sender, EventArgs e)
        {
            this._StateManager.ChangeState(States.ROOM_MANAGER);
        }

        private void Fullscreen_click(object sender, EventArgs e)
        {
            _StateManager._fullScreen = !_StateManager._fullScreen;
            if (_StateManager._fullScreen)
            {
                _graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width ;
                _graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
                _graphics.ApplyChanges();
            }
            else
            {
                _graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width/2;
                _graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height/2;
                _graphics.ApplyChanges();
            }
            this._StateManager.ChangeState(States.MAIN_MENU);


        }

        private void ExitGame_click(object sender, EventArgs e)
        {
            Communicator.stop();
            _game.Exit();
        }

        

        public override void update(GameTime gameTime)
        {     
            foreach ( var button in buttons)
            {
                button.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();
            
            foreach (var button in buttons)
                button.Draw(_spriteBatch);

            _spriteBatch.End();   
        }
    }
}
