﻿//#define debug

using UnderWorld.Backend;
using UnderWorld.DrawableObjects;
using UnderWorld.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using MonoGame.Penumbra.WindowsDX;
using Penumbra;
using System.Threading;
namespace UnderWorld.GameStates
{
    public class InGame : State
    {
        private readonly object _SpritesLock = new object();
        private readonly object _moveLock = new object();

        private bool firstLoad = true;
        private Camera _camera;
        private Sprite _map;
        private Sprite _testobj;
        private const int GameObjectsScaling = 200;
        private const int MIN_RANGE_KILL = 150;
        private LightingManager _lightMan;
        private MiniGameManager _MiniMan;
        private int[,] MapIdTable;
        private InteractableManager _interMan;
        private Dictionary<string, Animation> _animations;
        private Dictionary<string, Animation> _animationsMiner;
        Texture2D playerTexture;
        List<Sprite> _sprites;
        List<Rectangle> PlayerCollidable;
        private TextManager _textManager;
        Light _UserLight = new PointLight()
        {
            Scale = new Vector2(550.0f),
            ShadowType = ShadowType.Solid,
            Color = Color.Pink,
            Intensity = 1F,

        };


        public void transform()
        {

        }
        public InGame(StateManager man) : base(man)
        {
            Load();
        }

        public override void Draw(GameTime gameTime)
        {

            //
            //penumbra.BeginDraw();
            _spriteBatch.Begin(transformMatrix: _camera.Transform, samplerState: SamplerState.PointClamp);

            _map.Draw(_spriteBatch);
            lock(_moveLock)
            {
                _textManager.Draw(_spriteBatch);

                foreach (var sprite in _sprites)
                    sprite.Draw(_spriteBatch);
            }
            

            _spriteBatch.End();
            
            //penumbra.Draw(gameTime);

        }
        public override void DrawAbove(GameTime gameTime)
        {
            _MiniMan.DrawAbove(gameTime);
        }
        public override void Load()
        {
            Communicator.move += Communicator_move;
            //_StateManager._graphics.GraphicsProfile = GraphicsProfile.HiDef;
            //_StateManager._penumbra.Debug = true;
            _lightMan = new LightingManager(ref _StateManager._penumbra);
            _lightMan.addLightStatic(_UserLight, "userlight");
            _MiniMan = new MiniGameManager(_StateManager._spriteBatch, _StateManager._Game, _StateManager._graphics, _StateManager._comms, _StateManager._penumbra);
            _interMan = new InteractableManager();



            #region create_and_load_players
            _animations = new Dictionary<string, Animation>()
            {
                { "WalkUp", new Animation(_game.Content.Load<Texture2D>("Animations/WalkingUp"), 4, Color.White)},
                { "WalkDown", new Animation(_game.Content.Load<Texture2D>("Animations/WalkingDown"), 4, Color.White)},
                { "WalkLeft", new Animation(_game.Content.Load<Texture2D>("Animations/WalkingLeft"), 4, Color.White)},
                { "WalkRight", new Animation(_game.Content.Load<Texture2D>("Animations/WalkingRight"), 4, Color.White)},
                { "StopLeft", new Animation(_game.Content.Load<Texture2D>("Animations/StopLeft"), 1, Color.White)},
                { "StopRight", new Animation(_game.Content.Load<Texture2D>("Animations/StopRight"), 1, Color.White)},

            };
            
            _animationsMiner = new Dictionary<string, Animation>()
            {
                { "WalkUp", new Animation(_game.Content.Load<Texture2D>("Animations/MinerWalkingUp"), 4, Color.White)},
                { "WalkDown", new Animation(_game.Content.Load<Texture2D>("Animations/MinerWalkingDown"), 4, Color.White)},
                { "WalkLeft", new Animation(_game.Content.Load<Texture2D>("Animations/MinerWalkingLeft"), 4, Color.White)},
                { "WalkRight", new Animation(_game.Content.Load<Texture2D>("Animations/MinerWalkingRight"), 4, Color.White)},
                { "StopLeft", new Animation(_game.Content.Load<Texture2D>("Animations/MinerStopLeft"), 1, Color.White)},
                { "StopRight", new Animation(_game.Content.Load<Texture2D>("Animations/MinerStopRight"), 1, Color.White)},

            };
            _textManager = new TextManager(this._game.Content.Load<SpriteFont>("Fonts/MainFont"));


            #region game_objects
            _camera = new Camera(_graphics.PreferredBackBufferWidth, _graphics.PreferredBackBufferHeight);
            _map = new Sprite(this._game.Content.Load<Texture2D>("MapFiles\\Map"))
            {
                Color = Color.White,
                Position = new Vector2(0, 0),
                size = GameObjectsScaling,
            };

            _testobj = new Sprite(this._game.Content.Load<Texture2D>("Controls\\Button"))
            {
                Position = new Vector2(400, 400),
                size = 60,


            };
            #endregion

            PlayerCollidable = makeCollidables(_map, "Content\\MapFiles\\MapDescriber.json");
            #endregion

            reload();
            Player curPlayer = new Player(_animations)
            {
                InputKeys = new Input()
                {
                    up = Keys.W,
                    down = Keys.S,
                    left = Keys.A,
                    right = Keys.D,
                    interact = Keys.E,
                },
                Position = new Vector2(800, 800),
                Color = Color.Blue,
                speed = 4,
                size = GameObjectsScaling,
                isUser = true,
                Collision = PlayerCollidable,
                name = Communicator.GetUserLocation().username,

            };
            curPlayer.interact += CurPlayer_interact;
            _sprites = new List<Sprite>()
            {
                curPlayer
            };

            if (Communicator.hasPreviousLocation)
            {
                _sprites[0].Position = new Vector2(Communicator.GetUserLocation().x, Communicator.GetUserLocation().y);
            }
            this._textManager.AddTextObject("static",
                
                new Vector2(_sprites[0].Position.X - _sprites[0].Rectangle.Width / 2, _sprites[0].Position.Y - _sprites[0].Rectangle.Height / 2),
                "xyloc");

            this._textManager.AddTextObject("fps",

                new Vector2(_sprites[0].Position.X - _sprites[0].Rectangle.Width / 2, _sprites[0].Position.Y - _sprites[0].Rectangle.Height / 2),
                "fps");
            _textManager.AddTextObject(Communicator.statusText, new Vector2(), "GameStatus");

            if (Communicator.isRoomOwner)
            {
                this._textManager.AddTextObject("Press enter to start game!",
                      new Vector2(),
                      "StartGame");
                
            }

        }

        private void Communicator_move(object sender, EventArgs e)
        {
            lock (_moveLock)
            {
                Communicator.gameStarted = true;

                Communicator.statusText = "enjoy!";
                _textManager.UpdateSingle("StartGame", "");
                //Thread.Sleep(50);
                List<user> users = (List<user>)sender;
                foreach (var user in users)
                {
                    if (user.username != "")
                    {
                        foreach (var sprite in _sprites)
                        {
                            if (sprite.name == user.username)
                            {
                                sprite.Position = new Vector2(user.x, user.y);
                                sprite.animation = user.animation;
                                _textManager.UpdateSingle("" + user.username, new Vector2(user.x, user.y - _textManager.getFontHeight()));
                                sprite.alive = (user.alivestate);
                            }
                        }
                    }

                }
                List<user> curUsersConnected;
                lock (Communicator._object)
                {
                    curUsersConnected = new List<user>(Communicator.GetAllUsers());
                }
                updateAllPlayersAnimation(curUsersConnected);
                //Thread.Sleep(50);
            }
        }


        private void CurPlayer_interact(object sender, EventArgs e)
        {
            if (_sprites[0].isImpostor)
            {   int playerToKill = getPlayerInRange(MIN_RANGE_KILL);
                if (playerToKill != -1)
                {
                    Communicator.Kill(_sprites[playerToKill].name);
                }
            }
            else
            {
                INTERACTABLE_ID cur = _interMan.getInRange(_sprites[0].Position);
                
                switch (cur)
                {
                    case INTERACTABLE_ID.NONE:
                        return;
                        
                    case INTERACTABLE_ID.MINETHEORE:
                        _StateManager.ChangeState(States.MINE_ORE);
                        _interMan.setAsDone(INTERACTABLE_ID.MINETHEORE);
                        break;
                    case INTERACTABLE_ID.CATCHTHEORE:
                        _StateManager.ChangeState(States.CATCH_THE_ORE);
                        _interMan.setAsDone(INTERACTABLE_ID.CATCHTHEORE);
                        break;
                    case INTERACTABLE_ID.SORTTHEORE:
                        _StateManager.ChangeState(States.SORT_ORES);
                        _interMan.setAsDone(INTERACTABLE_ID.SORTTHEORE);
                        break;
                }

            }
        }

        bool classifyCollidable (int val)
        {
            List<int> collidables = new List<int>{ 11,60,61,62};
            return collidables.Contains(val);

        }
        bool classifyLights(int val)
        {
            List<int> Lightabls = new List<int> { 34,27 };
            return Lightabls.Contains(val);

        }

        int tileWidth = 0;
        int tileHeight = 0;
        List<Rectangle> makeCollidables( Sprite map , string mapDescriptor,bool onlyHulls = false)
        {
            MapJson mapData;
            using (StreamReader r = new StreamReader(mapDescriptor))
            {
                string json = r.ReadToEnd();
                mapData= JsonConvert.DeserializeObject<MapJson>(json);
            }
            tileWidth = map.Rectangle.Width / mapData.width;
            tileHeight = map.Rectangle.Height / mapData.height;
            int mapWidth = mapData.width;
            int mapHeight = mapData.height;
            var res = new List<Rectangle>();
            int num = 0;
            MapIdTable = new int[mapWidth, mapHeight];

            for (int x = 0; x < mapWidth; x++)
            {
                for (int y = 0; y < mapWidth; y++)
                {
                    int index = x + y * mapWidth;
                    int curTile = mapData.layers[0].data[index];
                    MapIdTable[x, y] = curTile;


                    if (classifyCollidable(curTile))
                    {
                        if (x == 0 && y == 0)
                        {
                            num += 1;
                        }
                        
                        if (!onlyHulls)
                        {
                            res.Add(new Rectangle(map.Rectangle.X + x * tileWidth, map.Rectangle.Y + y * tileHeight, tileWidth, tileHeight));
                            var cur = new Hull(new Vector2(1.0f), new Vector2(-1.0f, 1.0f), new Vector2(-1.0f), new Vector2(1.0f, -1.0f))
                            {
                                Position = new Vector2(map.Rectangle.X + x * tileWidth, map.Rectangle.Y + y * tileHeight),
                                Scale = new Vector2(tileWidth),
                                Enabled = true,

                            };
                            _lightMan.addHullGlobal(cur, x + "," + y);
                        }
                        //_StateManager._penumbra.Hulls.Add(cur);
                    }
                    if ( classifyLights (curTile) && !onlyHulls)
                    {
                        //res.Add(new Rectangle(map.Rectangle.X + x * tileWidth, map.Rectangle.Y + y * tileHeight, tileWidth, tileHeight));
                        Light cur = new PointLight()
                        {
                            Enabled = true,
                            Position = new Vector2(map.Rectangle.X + (x-1) * tileWidth + (tileWidth / 2), map.Rectangle.Y + (y-1) * tileHeight + (tileHeight / 2)),
                            Scale = new Vector2(30.0f),
                            ShadowType = ShadowType.Solid,
                            Intensity = 0.3F,
                            Color = Color.Pink,
                        };

                        _lightMan.addLightGlobal(cur, x + "," + y);

                    }
                    if(!onlyHulls &&
                        (x == 38 && y == 7))
                    {
                        _interMan.addInteractable(new InteractionObject(INTERACTABLE_ID.MINETHEORE, new Vector2(map.Rectangle.X + (x - 1) * tileWidth + (tileWidth / 2), map.Rectangle.Y + (y - 1) * tileHeight + (tileHeight / 2))));

                    }
                    if (!onlyHulls &&
                        (x == 64 && y == 96))
                    {
                        _interMan.addInteractable(new InteractionObject(INTERACTABLE_ID.CATCHTHEORE, new Vector2(map.Rectangle.X + (x - 1) * tileWidth + (tileWidth / 2), map.Rectangle.Y + (y - 1) * tileHeight + (tileHeight / 2))));

                    }
                    if (!onlyHulls &&
                        (x == 70 && y == 38))
                    {
                        _interMan.addInteractable(new InteractionObject(INTERACTABLE_ID.SORTTHEORE, new Vector2(map.Rectangle.X + (x - 1) * tileWidth + (tileWidth / 2), map.Rectangle.Y + (y - 1) * tileHeight + (tileHeight / 2))));

                    }

                }
            }
            _lightMan.enabled = true;

            return res ;
        }
        public override void reload()
        {
            _StateManager._penumbra.Enabled = true;

            _StateManager._penumbra.Visible = true;
            _StateManager._penumbra.Lights.Clear();
            _StateManager._penumbra.Lights.Add(_UserLight);
            /*_StateManager._penumbra.Lights.Add(new PointLight()
            {
                Position = new Vector2(),
                Scale = new Vector2(700.0f),
                Color = Color.Pink,
                ShadowType = ShadowType.Solid,

            });*/
            _StateManager._penumbra.Hulls.Clear();
            _lightMan.remake();
            makeCollidables(_map, "Content\\MapFiles\\MapDescriber.json",true);


            List<user> curUsersConnected;
            lock (Communicator._object)
            {
                curUsersConnected = new List<user>(Communicator.GetAllUsers());
            }
            updateAllPlayersAnimation(curUsersConnected);


        }





        public int getPlayerInRange(float range)
        {
            float closest = 10000000000000;
            int closestId = -1;
            if (_sprites.Count> 1)
            {
                
                for (int i = 1; i < _sprites.Count; i++)
                { 
                    float dis = InteractableManager.distance(_sprites[0].Position.X, _sprites[0].Position.Y, _sprites[i].Position.X, _sprites[i].Position.Y);
                    if (dis < closest && dis <range && !_sprites[i].isImpostor)
                    {
                        closest = dis;
                        closestId = i;
                    }
                }                
            }
            return closestId;
        }


        public void updateAllPlayersAnimation(List<user> curUsersConnected)
        {
            if (_sprites == null)
                return;
            lock(_SpritesLock) 
            { 
            try
            {
                _textManager.RemoveAll();
                _sprites.RemoveAll(item =>
                {
                    return !item.isUser;
                });
                foreach (var user in curUsersConnected)
                {
                    Dictionary<string, Animation> animationsKiller = new Dictionary<string, Animation>()
                            {
                                { "WalkUp", new Animation(_game.Content.Load<Texture2D>("Animations/WalkingUp"), 4, Color.White)},
                                { "WalkDown", new Animation(_game.Content.Load<Texture2D>("Animations/WalkingDown"), 4, Color.White)},
                                { "WalkLeft", new Animation(_game.Content.Load<Texture2D>("Animations/WalkingLeft"), 4, Color.White)},
                                { "WalkRight", new Animation(_game.Content.Load<Texture2D>("Animations/WalkingRight"), 4, Color.White)},
                                { "StopLeft", new Animation(_game.Content.Load<Texture2D>("Animations/StopLeft"), 1, Color.White)},
                                { "StopRight", new Animation(_game.Content.Load<Texture2D>("Animations/StopRight"), 1, Color.White)},

                            };
                    Dictionary<string, Animation> animationsMiner = new Dictionary<string, Animation>()
                            {
                                 { "WalkUp", new Animation(_game.Content.Load<Texture2D>("Animations/MinerWalkingUp"), 4, Color.White)},
                                 { "WalkDown", new Animation(_game.Content.Load<Texture2D>("Animations/MinerWalkingDown"), 4, Color.White)},
                                 { "WalkLeft", new Animation(_game.Content.Load<Texture2D>("Animations/MinerWalkingLeft"), 4, Color.White)},
                                 { "WalkRight", new Animation(_game.Content.Load<Texture2D>("Animations/MinerWalkingRight"), 4, Color.White)},
                                 { "StopLeft", new Animation(_game.Content.Load<Texture2D>("Animations/MinerStopLeft"), 1, Color.White)},
                                 { "StopRight", new Animation(_game.Content.Load<Texture2D>("Animations/MinerStopRight"), 1, Color.White)},

                            };
                    if (user.username == Communicator.GetUserLocation().username )
                    {

                        Player curPlayer = new Player(user.curuserrole == 0 ? animationsMiner : animationsKiller)
                        {
                            InputKeys = new Input()
                            {
                                up = Keys.W,
                                down = Keys.S,
                                left = Keys.A,
                                right = Keys.D,
                                interact = Keys.E,
                            },
                            Position = _sprites[0].Position,
                            Color = Color.Blue,
                            speed = 4,
                            size = GameObjectsScaling,
                            isUser = true,
                            alive = _sprites[0].alive,
                            animation = _sprites[0].animation,
                            Collision = PlayerCollidable,
                            isImpostor = !(user.curuserrole == 0),
                            name = Communicator.GetUserLocation().username,

                        };
                        curPlayer.interact += CurPlayer_interact;
                        _sprites[0] = curPlayer;



                        

                    }
                    if (user.username != Communicator.GetUserLocation().username && user.username != "")
                    {

                        var newPlayer = new Player(user.curuserrole == 0 ? animationsMiner : animationsKiller)
                        {
                            Position = new Vector2(user.x, user.y),
                            animation = user.animation,
                            size = GameObjectsScaling,
                            name = user.username,
                            Collision = PlayerCollidable,
                            isImpostor = !(user.curuserrole == 0)

                        };


                        _sprites.Add(newPlayer);
                        _textManager.AddTextObject("" + user.username, new Vector2(user.x, user.y - _textManager.getFontHeight()), "" + user.username);

                    }
                }
            }catch( Exception e)
            {

            }
            }   
            
        }

        public override void update(GameTime gameTime)
        {
            lock (_moveLock)
            {
                bool isCurUserAlive = true;

                //_StateManager._penumbra.Debug = true;
                _StateManager._penumbra.Visible = true;
                _lightMan.update(gameTime, (_sprites[0].Position - new Vector2(_graphics.PreferredBackBufferWidth / 2, _graphics.PreferredBackBufferHeight / 2)));
                //_lightMan.update(gameTime, _sprites[0].Position );

                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                {

                    this._StateManager.ChangeState(States.PAUSE);
                }
                else if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.J))
                {
                    _StateManager.ChangeState(States.CATCH_THE_ORE);

                }
                else if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.K))
                {
                    _StateManager.ChangeState(States.MINE_ORE);

                }
                else if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.L))
                {
                    _StateManager.ChangeState(States.SORT_ORES);

                }
                else if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Enter))
                {
                    if (Communicator.isRoomOwner && !Communicator.gameStarted)
                    {
                        Communicator.Start();
                    }
                }
                else
                {
                    


                    _textManager.RemoveTextObject("xyloc");
                    _textManager.RemoveTextObject("fps");
                    _textManager.RemoveTextObject("blockId");
                    _textManager.RemoveTextObject("GameStatus");

                    //
                    this._textManager.AddTextObject(Communicator.statusText,
                    new Vector2(),
                    "GameStatus");


#if debug
                    this._textManager.AddTextObject("static",
                    new Vector2(),
                    "xyloc");

                    this._textManager.AddTextObject("fps",
                        new Vector2(),
                        "fps");
                    this._textManager.AddTextObject("blockId",
                        new Vector2(),
                        "blockId");
#endif


                    if (Communicator.isRoomOwner)
                    {
                        _textManager.RemoveTextObject("StartGame");
                        if (!Communicator.gameStarted)
                        {
                            this._textManager.AddTextObject("Press enter to start game!",
                            new Vector2(),
                            "StartGame");
                        }
                        _textManager.UpdateSingle("StartGame",
                            "Press enter to start game!",
                            new Vector2(_sprites[0].Position.X + _sprites[0].SpritWidth / 2 - (_graphics.PreferredBackBufferWidth / 2) + 100,
                                        _sprites[0].Position.Y + _sprites[0].SpritHeight / 2 - (_graphics.PreferredBackBufferHeight / 2) + 175));
                    }



                    _textManager.UpdateSingle("xyloc",
                        "x,y: " + (int)(_sprites[0].Position.X / tileWidth) + "," + ((int)(_sprites[0].Position.Y / tileHeight) + 1),
                        new Vector2(_sprites[0].Position.X + _sprites[0].SpritWidth / 2 - (_graphics.PreferredBackBufferWidth / 2) + 100,
                                    _sprites[0].Position.Y + _sprites[0].SpritHeight / 2 - (_graphics.PreferredBackBufferHeight / 2) + 100));
                    _textManager.UpdateSingle("fps",
                        "fps: " + (int)(1 / gameTime.ElapsedGameTime.TotalSeconds),
                        new Vector2(_sprites[0].Position.X + _sprites[0].SpritWidth / 2 - (_graphics.PreferredBackBufferWidth / 2) + 100,
                                    _sprites[0].Position.Y + _sprites[0].SpritHeight / 2 - (_graphics.PreferredBackBufferHeight / 2) + 125));
                    _textManager.UpdateSingle("blockId",
                        "Block ID: " + MapIdTable[(int)(_sprites[0].Position.X / tileWidth), (int)(_sprites[0].Position.Y / tileHeight) + 1],
                        new Vector2(_sprites[0].Position.X + _sprites[0].SpritWidth / 2 - (_graphics.PreferredBackBufferWidth / 2) + 100,
                                    _sprites[0].Position.Y + _sprites[0].SpritHeight / 2 - (_graphics.PreferredBackBufferHeight / 2) + 150));
                    _textManager.UpdateSingle("GameStatus", Communicator.statusText, new Vector2(_sprites[0].Position.X + _sprites[0].SpritWidth / 2 - (_graphics.PreferredBackBufferWidth / 2) + 100,
                                                                       _sprites[0].Position.Y + _sprites[0].SpritHeight / 2 - (_graphics.PreferredBackBufferHeight / 2) + 50));


                    if (_StateManager._fullScreen)
                    {
                        _camera.FollowFullSceen(_sprites[0]);
                        _UserLight.Position = new Vector2(_graphics.PreferredBackBufferWidth / 2, _graphics.PreferredBackBufferHeight / 2);

                    }
                    else
                    {
                        _camera.Follow(_sprites[0]);
                        _UserLight.Position = new Vector2(_graphics.PreferredBackBufferWidth / 2, _graphics.PreferredBackBufferHeight / 2);
                    }

                    Communicator.SetUserLocation(_sprites[0].Position, _sprites[0].animation);

                    #region update sprite count


                    int userCount = Communicator.connectedUsers;
                    List<user> curUsersConnected;
                    lock (Communicator._object)
                    {
                        curUsersConnected = new List<user>(Communicator.GetAllUsers());
                    }

                    if (_sprites.Count != userCount)
                    {
                        updateAllPlayersAnimation(curUsersConnected);
                    }
                    #endregion

                    #region update players location 
                    foreach (var user in curUsersConnected)
                    {
                        if (user.username != "")
                        {

                            if (user.username != Communicator.GetUserLocation().username)
                            {
                                foreach (var sprite in _sprites)
                                {
                                    if (sprite.name == user.username)
                                    {
                                        sprite.Position = new Vector2(user.x, user.y);
                                        sprite.animation = user.animation;
                                        _textManager.UpdateSingle("" + user.username, new Vector2(user.x, user.y - _textManager.getFontHeight()));
                                        sprite.alive = (user.alivestate);
                                    }
                                }
                            }
                            else
                            {
                                isCurUserAlive = user.alivestate ==1 ;
                            }
                        }
                        if (user.action == 100)
                        {
                            _textManager.UpdateSingle("GameStatus", "MINERS WIN!");
                            Communicator.statusText = "MINERS WIN! - Create new room to restart.";
                        }
                        if (user.action == 200)
                        {
                            _textManager.UpdateSingle("GameStatus", "IMPOSTORS WIN!");
                            Communicator.statusText = "IMPOSTORS WIN! - Create new room to restart.";
                        }
                    }
                    lock (_SpritesLock)
                    {
                        foreach (var sprite in _sprites)
                        {
                            sprite.Update(gameTime);
                        }
                    }

                    #endregion

                    #region updateIfDead

                    if (!isCurUserAlive)
                        Communicator.statusText = "YOU DIED - CONTINUE DOING YOUR TASKS TO WIN";
                    #endregion

                        #region update In range


                    int closestId = getPlayerInRange(MIN_RANGE_KILL);
                    for (int i = 0; i < _sprites.Count; i++)
                    {

                        if (i == closestId)
                        {
                            _sprites[i].Color = Color.Red;
                        }
                        else
                        {
                            _sprites[i].Color = Color.White;

                        }

                    }
                    #endregion

                }

            }
        }

    }
}
