﻿using UnderWorld.Backend;
using UnderWorld.DrawableObjects;
using UnderWorld.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;
using UnderWorld.Controls;


namespace UnderWorld.GameStates
{
    class CreateRoom : State
    {
        private int _maxPlayers = 10;
        private string _roomName = Communicator.curUser + "'s room";
        TextManager _textMan;
        private List<Component> components = new List<Component>();
        public TextBox RoomName;
        public TextBox MaxPlayers;
        public CreateRoom(StateManager man) : base(man)
        {
            Load();
        }

        public override void Load()
        {
            _textMan = new TextManager(_game.Content.Load<SpriteFont>("Fonts/ButtonFont"));

            _textMan.AddTextObject("Room Name:", new Vector2(this._graphics.PreferredBackBufferWidth / 2 - 100, this._graphics.PreferredBackBufferHeight / 2 - 200), "roomName");
            RoomName = new TextBox(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"),
                       base._StateManager._Game)
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 - 130),
                text = _roomName,
                PenColor = Color.Black,
            };
            _textMan.AddTextObject("Max Players:", new Vector2(this._graphics.PreferredBackBufferWidth / 2 - 100, this._graphics.PreferredBackBufferHeight / 2 - 80), "MaxPlayers");

            MaxPlayers = new TextBox(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"),
                       base._StateManager._Game)
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 - 10),
                text = _maxPlayers.ToString(),
                PenColor = Color.Black,
                numbersOnly = true,
            };

            Button Create = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 + 70),
                text = "Create",
                PenColor = Color.Black,
            };
            Button backToRoomList = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 + 150),
                text = "Back",
                PenColor = Color.Black,
            };
            _textMan.AddTextObject("", new Vector2(this._graphics.PreferredBackBufferWidth / 2 - 200, this._graphics.PreferredBackBufferHeight / 2 + 200), "error");

            RoomName.EnterPressed += Username_EnterPressed;
            MaxPlayers.EnterPressed += Password_EnterPressed;
            Create.click += Create_click; ;
            backToRoomList.click += BackToRoomList_click; ;



            components = new List<Component>()
            {
                RoomName,
                MaxPlayers,
                Create,
                backToRoomList,
            };

        }

        private void Create_click(object sender, EventArgs e)
        {
            _maxPlayers = Convert.ToInt32(MaxPlayers.text);
            _roomName = RoomName.text;
            if (_maxPlayers > 4 && _roomName != "")
            {
                string res = Communicator.createRoom(new CreateRoomRequest(_roomName, _maxPlayers));
                if (res == "1")
                {
                    _StateManager.ChangeState(States.INGAME);

                }
                else
                {
                    _textMan.UpdateSingle("error", res);
                }

            }
        }

        private void BackToRoomList_click(object sender, EventArgs e)
        {
            _StateManager.ChangeState(States.ROOM_MANAGER);
        }



        private void Password_EnterPressed(object sender, EventArgs e)
        {
        }

        private void Username_EnterPressed(object sender, EventArgs e)
        {
        }

        public override void update(GameTime gameTime)
        {
            foreach (var component in components)
            {
                component.Update(gameTime);
            }
        }
        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();

            foreach (var component in components)
                component.Draw(_spriteBatch);
            _textMan.Draw(_spriteBatch);

            _spriteBatch.End();
        }
    }
}
