﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using UnderWorld.Backend;
using UnderWorld.Controls;
using UnderWorld.Managers;

namespace UnderWorld.GameStates
{
    class SignupMenu : State
    {
        TextManager _textMan;
        private List<Component> components = new List<Component>();
        public TextBox username;
        public TextBox password;
        public SignupMenu(StateManager man) : base(man)
        {
            Load();
        }

        public override void Load()
        {
            _textMan = new TextManager(_game.Content.Load<SpriteFont>("Fonts/ButtonFont"));

            _textMan.AddTextObject("Username:", new Vector2(this._graphics.PreferredBackBufferWidth / 2 - 100, this._graphics.PreferredBackBufferHeight / 2 - 200), "username");
            username = new TextBox(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"),
                       base._StateManager._Game)
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 - 130),
                text = "",
                PenColor = Color.Black,
            };
            _textMan.AddTextObject("Password:", new Vector2(this._graphics.PreferredBackBufferWidth / 2 - 100, this._graphics.PreferredBackBufferHeight / 2 - 80), "password");

            password = new TextBox(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"),
                       base._StateManager._Game)
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 - 10),
                text = "",
                PenColor = Color.Black,
            };

            Button SignUp = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 + 70),
                text = "SignUp",
                PenColor = Color.Black,
            };

            Button StartUp = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 + 150),
                text = "Back",
                PenColor = Color.Black,
            };
            _textMan.AddTextObject("", new Vector2(this._graphics.PreferredBackBufferWidth / 2 - 200, this._graphics.PreferredBackBufferHeight / 2 + 200), "error");


            username.EnterPressed += Username_EnterPressed;
            password.EnterPressed += Password_EnterPressed;
            SignUp.click += SignUp_click; ;
            StartUp.click += StartUp_click; ;



            components = new List<Component>()
            {
                username,
                password,
                SignUp,
                StartUp
            };

        }

        private void SignUp_click(object sender, EventArgs e)
        {
            if (password.text != "" && username.text != "")
            {
                string res = Communicator.Signup(username.text, password.text, "doc@gmail.com");
                
                if (res == "1")
                {
                    _StateManager.ChangeState(States.MAIN_MENU);

                }
                else
                {
                    _textMan.UpdateSingle("error", res);
                }
            }     
        }

        private void StartUp_click(object sender, EventArgs e)
        {
            _StateManager.ChangeState(States.STARTUP);
        }

        private void Password_EnterPressed(object sender, EventArgs e)
        {
            
        }

        private void Username_EnterPressed(object sender, EventArgs e)
        {
            
        }

        public override void update(GameTime gameTime)
        {
            foreach (var component in components)
            {
                component.Update(gameTime);
            }
        }
        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();

            foreach (var component in components)
                component.Draw(_spriteBatch);
            _textMan.Draw(_spriteBatch);

            _spriteBatch.End();
        }

    }
}
