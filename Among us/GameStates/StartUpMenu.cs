﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;
using UnderWorld.Backend;
using UnderWorld.Controls;
using UnderWorld.Managers;

namespace UnderWorld.GameStates
{
    class StartUpMenu : State
    {
        private List<Component> buttons;

        public StartUpMenu(StateManager man) : base(man)
        {
            Load();
        }

        public override void Load()
        {
            Button login = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 - 60),
                text = "Login",
                PenColor = Color.Black,
            };

            Button signup = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 + 20),
                text = "Signup",
                PenColor = Color.Black,
            };
            Button options = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                      _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 + 100),
                text = "Options",
                PenColor = Color.Black,
            };
            Button exit = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                      _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2(this._graphics.PreferredBackBufferWidth / 2, this._graphics.PreferredBackBufferHeight / 2 + 100),
                text = "Exit",
                PenColor = Color.Black,
            };


            login.click += Login_click;
            signup.click += Signup_click;
            options.click += Options_click;
            exit.click += Exit_click;

            buttons = new List<Component>()
            {
                login,
                signup,
                exit

            };
        }

        private void Exit_click(object sender, EventArgs e)
        {
            Communicator.stop();
            _game.Exit();
        }

        private void Options_click(object sender, EventArgs e)
        {
            _StateManager.ChangeState(States.OPTIONS);
        }

        private void Signup_click(object sender, EventArgs e)
        {
            _StateManager.ChangeState(States.SIGNUP);
        }

        private void Login_click(object sender, EventArgs e)
        {
            _StateManager.ChangeState(States.LOGIN);
        }

        public override void update(GameTime gameTime)
        {
            foreach (var button in buttons)
            {
                button.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();

            foreach (var button in buttons)
                button.Draw(_spriteBatch);

            _spriteBatch.End();

        }
    }
}
