﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using Microsoft.Xna.Framework;
using System.Threading;
using  UnderWorld.Managers;
using System.Diagnostics;
using System.IO;

namespace UnderWorld.Backend
{
    public class user
    {
        public user()
        {
            x = 0;
            y = 0;
            animation = 0;
            username = "";
            action = 0;
            curaction = "";

            alivestate = 1;
            curuserrole = 0;
            //iscuruser = false;

        }

        public float x 
        {
            get;
            set;
        } = 0;
        public float y
        {
            get;
            set;
        } = 0;
        public int animation 
        {
            get;
            set;
        } = 0;
        public string username
        {
            get;
            set;
        } = "";
        public int action
        {
            get;
            set;
        } = 0;
        public string curaction
        {
            get;
            set;
        } = "";

        public int alivestate
        {
            get;
            set;
        } = 1;
        public int curuserrole
        {
            get;
            set;
        } = 0;
        /*public bool iscuruser
        {
            get;
            set;
        } = true;*/

    }
    public class Communicator
    {
        public static event EventHandler move;
        private static string IP = "127.0.0.1";
        #region constants
        public const int HEADER_SIZE = 5;
        private const int MAX_PLAYERS = 20;
        enum returnCode {
            OK,
            ALREADY_LOGGED_IN,
            INVALID_PASS,
            USER_DOES_NOT_EXIST,
            USERNAME_TAKEN,
            USERNAME_NOT_CONNECTED
        };
        #endregion
        public static readonly object _object = new object();
        public static bool isRoomOwner = false;
        public static TcpClient soc;
        public static NetworkStream socStream;
        public static string curUser = "";
        public static int curRoomid = -1, curRoomMaxPlayers;
        public static string curRoomName = "";
        public static bool ingame;
        public static bool hasPreviousLocation = false;
        private static user userLocation ;
        public static Thread thread;
        public static List<user> allUsers;
        public static int connectedUsers;
        public static bool gameStarted = false;
        public static string statusText = "waiting for game to start...";

        static Communicator()
        {

            StreamReader sr = new StreamReader("config.txt");
            IP = sr.ReadLine();
            sr.Close();


            Connect();
            userLocation = new user();

        }

      


        private static void CommunicationThread()
        {
            ingame = true;
           
            while (ingame)
            {
                try
                {
                    byte[] q = Serializer.Serialize(userLocation);
                    socStream.Write(q, 0, q.Length);
                    socStream.Flush();


                    //reset action
                    //userLocation.curaction = "";
                    userLocation.action = 0;


                    //done

                    var data = getDataFromSoc();
                    if (data.Item1 == 99)
                        throw new System.ArgumentException(Deserializer.DeserializeErrorResponse(data.Item2).message, "original");
                    string usersData = Deserializer.DeserializePlayerStatus(data.Item2).PlayersInRoom;
                    var userList = usersData.Split(";");
                    connectedUsers = userList.Length;
                    bool needToMove = false ;
                    Monitor.Enter(_object);
                    try
                    {
                        allUsers = new List<user>(MAX_PLAYERS);
                        for (var i = 0; i < MAX_PLAYERS; i++)
                            allUsers.Add(new user());
                        for (int i = 0; i < userList.Length; i++)
                        {

                            user temp = new user();
                            var curUser = userList[i].Split(",");
                            temp.username = curUser[0];
                            temp.x = float.Parse(curUser[1]);
                            temp.y = float.Parse(curUser[2]);
                            temp.animation = int.Parse(curUser[3]);
                            temp.action = int.Parse(curUser[4]);
                            temp.alivestate = int.Parse(curUser[5]);
                            temp.curuserrole = int.Parse(curUser[6]);
                            
                            //temp.iscuruser = bool.Parse(curUser[7]);
                            temp.curaction = curUser[8];
                            //if (temp.curaction != "")
                              //  Debug.WriteLine(temp.curaction);

                            if (temp.action == 50)
                            {
                                needToMove = true;
                                temp.action = 0;
                            }
                            allUsers[i] = temp;


                            


                        }


                    }
                    finally
                    {
                        Monitor.Exit(_object);
                    }
                    if (needToMove && move != null)
                    {
                        move?.Invoke(allUsers, null);




                    }

                }
                catch
                {

                }
                

                

            }
            

        }

    

        public static List<user> GetAllUsers()
        {
            return allUsers;
        }

        
        public static void SetUserLocation(Vector2 loc, int animation)
        {
            hasPreviousLocation = true;
            userLocation.x = loc.X;
            userLocation.y = loc.Y;
            userLocation.animation = animation;
        }
        public static void Kill(string toKill)
        {
            userLocation.action = 1;
            userLocation.curaction = toKill;
        }
        public static void Start()
        {
            userLocation.action = 2;
            userLocation.curaction = "StartGame";
        }
        public static void FinishTask(INTERACTABLE_ID id)
        {
            userLocation.action = 3;
            userLocation.curaction = ""+ (int)id;
        }
        public static user GetUserLocation()
        {
            return userLocation;
        }
        public static void stop()
        {
            ingame = false;
        }


        public static bool Connect()
        {
            soc = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(IP), 8826);
            
            try
            {
                soc.Connect(serverEndPoint);
                socStream = soc.GetStream();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected exception : {0}", e.ToString());
                return false;
            }
            return true;
        }
        public static string signout()
        {
            byte[] q = Serializer.serializeLogOut();
            socStream.Write(q, 0, q.Length);
            socStream.Flush();

            var data = getDataFromSoc();
            if (data.Item1 == 99)
                return Deserializer.DeserializeErrorResponse(data.Item2).message;
            return Deserializer.DeserializeLogOutResponse(data.Item2).status + "";
        }


        public static string LeaveRoom()
        {
            gameStarted = false;
            hasPreviousLocation = false;
            byte[] q = Serializer.serializeLogOut();
            socStream.Write(q, 0, q.Length);
            socStream.Flush();
            isRoomOwner = false;

            var data = getDataFromSoc();
            if (data.Item1 == 99)
                return Deserializer.DeserializeErrorResponse(data.Item2).message;
            return Deserializer.DeserializeLogOutResponse(data.Item2).status + "";
        }





        public static Tuple<int, string[]> getrooms()
        {
            byte[] q = Serializer.serializeRooms();
            socStream.Write(q, 0, q.Length);
            socStream.Flush();
            var resData = getDataFromSoc();
            if (resData.Item1 == 99)
            {
                string[] errRes = { Deserializer.DeserializeErrorResponse(resData.Item2).message};

                return Tuple.Create(0, errRes);
            }
            return Tuple.Create(1,Deserializer.DeserializeGetRoomsRespones(resData.Item2).rooms);
        }


        public static string createRoom(CreateRoomRequest data)
        {
            byte[] q = Serializer.Serialize(data);
            socStream.Write(q, 0, q.Length);
            socStream.Flush();
            var resData = getDataFromSoc();
            if (resData.Item1 == 99)
                return Deserializer.DeserializeErrorResponse(resData.Item2).message;
            curRoomid = 0;
            curRoomName = data.roomName;
            
            curRoomMaxPlayers = data.maxUsers;

            thread = new Thread(new ThreadStart(CommunicationThread));
            thread.Start();
            isRoomOwner = true;
            statusText = "waiting for game to start...";
            return Deserializer.DeserializeSignUpResponse(resData.Item2).status + "";


        }


        /*function returns state of signup in a printable fashion
         */
        public static string Signup(string username, string password, string email)
        {
            SignUpRequest reqObj = new SignUpRequest(username, password, email);
            byte[] q = Serializer.Serialize(reqObj);
            socStream.Write(q, 0, q.Length);
            socStream.Flush();
            
            var data = getDataFromSoc();
            if (data.Item1 == 99)
                return Deserializer.DeserializeErrorResponse(data.Item2).message;
            curUser = username;
            userLocation.username = username;

            return Deserializer.DeserializeSignUpResponse(data.Item2).status+"";
        }

        /*function returns state of signin in a printable fashion
         */
        public static string Signin(string username, string password)
        {
            SignInRequest reqObj = new SignInRequest(username, password);
            byte[] q = Serializer.Serialize(reqObj);
            socStream.Write(q, 0, q.Length);
            socStream.Flush();
            
            var data = getDataFromSoc();
            if (data.Item1 == 99)
                return Deserializer.DeserializeErrorResponse(data.Item2).message;
            curUser = username;
            userLocation.username = username;
            return Deserializer.DeserializeSignUpResponse(data.Item2).status + "";
        }
        public static Tuple< int ,byte[]> getDataFromSoc()
        {
            //getting headers
            byte[] header = new byte[HEADER_SIZE];
            int bytesRead = socStream.Read(header, 0, HEADER_SIZE);
            if (bytesRead != HEADER_SIZE)
            {
                if (bytesRead == 1)
                    throw new System.ArgumentException("ERROR");
                else
                {
                    throw new System.ArgumentException("ERROR");
                }
                return Tuple.Create(1, header);

            }
            int code = Deserializer.getCode(header);
            int size = Deserializer.getLen(header);
            //getting data
            byte[] data = new byte[size];
            bytesRead = socStream.Read(data, 0, size);
            if (bytesRead != size)
            {
                return Tuple.Create(1, header);
            }

            return Tuple.Create(code, data);
        }

        public static string joinRoom(JoinRoomRequest req)
        {            
            byte[] q = Serializer.Serialize(req);
            socStream.Write(q, 0, q.Length);
            socStream.Flush();
            gameStarted = false;

            var data = getDataFromSoc();
            if (data.Item1 == 99)
                return Deserializer.DeserializeErrorResponse(data.Item2).message;

            curRoomName = req.roomName;
            thread = new Thread(new ThreadStart(CommunicationThread));
            thread.Start();
            isRoomOwner = false;
            statusText = "waiting for game to start...";
            return Deserializer.DeserializeJoinRoomResponse(data.Item2).status+"" ;
        }
        public static string getPersonalBest()
        {
            byte[] q = Serializer.serializeStats();
            socStream.Write(q, 0, q.Length);
            socStream.Flush();

            var data = getDataFromSoc();
            if (data.Item1 == 99)
                return Deserializer.DeserializeErrorResponse(data.Item2).message;
            var pb = Deserializer.DeserializeGetStatsRespones(data.Item2).self;
            return "total answers: "+ pb.totalAns+"\nnumber of games: "+pb.numGames+"\nnumbers of correct answers: "+ pb.numCorrect +"\navarage time: "+pb.avgTime;
        }
        public static string getWorldBest()
        {
            byte[] q = Serializer.serializeStats();
            socStream.Write(q, 0, q.Length);
            socStream.Flush();

            var data = getDataFromSoc();
            if (data.Item1 == 99)
                return Deserializer.DeserializeErrorResponse(data.Item2).message;
            var wb = Deserializer.DeserializeGetStatsRespones(data.Item2).top;
            return wb.FirstUser.Item1 + ": " + wb.FirstUser.Item2 + "\n" + wb.SecondUser.Item1 + ": " + wb.SecondUser.Item2 + "\n" + wb.ThirdUser.Item1 + ": " + wb.ThirdUser.Item2 + "\n";
        }

        public static string getRoomData()
        {
            byte[] q = Serializer.Serialize(new GetPlayersInRoomRequest(curRoomid));
            socStream.Write(q, 0, q.Length);
            socStream.Flush();

            var data = getDataFromSoc();
            if (data.Item1 == 99)
                return Deserializer.DeserializeErrorResponse(data.Item2).message;
            var res = Deserializer.DeserializeGetPlayersInRoomRespone(data.Item2);

            curRoomid = 0;
            curRoomName = res.Name;
            curRoomMaxPlayers = res.MaxPlayers;

            return string.Join("\n", res.players) ;
        }
    }
}
