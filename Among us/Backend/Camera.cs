﻿using UnderWorld.DrawableObjects;
using Microsoft.Xna.Framework;
using SharpDX.Direct2D1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnderWorld.Backend
{
    public class Camera
    {
        public Matrix Transform { get; private set; }
        public float ScreenWidth;
        public float ScreenHeight;

        public Camera(float ScreenWidth, float ScreenHeight)
        {
            this.ScreenWidth = ScreenWidth;
            this.ScreenHeight = ScreenHeight;
        }

        public void zeroOut( )
        {

            Transform = new Matrix() ;
        }


        
        public void FollowFullSceen(Sprite target)
        {
            var position = Matrix.CreateTranslation(
              -target.Position.X - target.Rectangle.Width / 2,
              -target.Position.Y - target.Rectangle.Height / 2,
              0);

            var offset = Matrix.CreateTranslation(
                ScreenWidth / 4,
                ScreenHeight / 4,
                0);
            var scale = Matrix.CreateScale(2.0f);
            Transform = position * offset * scale;
        }
        public void Follow(Sprite target)
        {
            var position = Matrix.CreateTranslation(
              -target.Position.X - target.Rectangle.Width / 2,
              -target.Position.Y - target.Rectangle.Height / 2,
              0);

            var offset = Matrix.CreateTranslation(
                ScreenWidth / 2,
                ScreenHeight / 2,
                0);

            Transform = position * offset;
        }
    }
}
