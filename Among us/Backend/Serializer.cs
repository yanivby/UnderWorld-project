﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace UnderWorld.Backend
{
    public class SignInRequest
    {
        public SignInRequest(string user, string pass)
        {
            username = user;
            password = pass;
        }
        public string username { get; set; }
        public string password { get; set; }
    }

    public class SignUpRequest
    {
        public SignUpRequest(string user, string pass,string email)
        {
            username = user;
            password = pass;
            this.email = email;
        }
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
    }

    public class JoinRoomRequest
    {
        public JoinRoomRequest(string name)
        {
            roomName = name;
        }
        public string roomName { get; set; }
    }
    
    public class CreateRoomRequest
    {
        public CreateRoomRequest(string name, int max)
        {
            roomName = name;
            maxUsers = max;
            
        }
        public string roomName { get; set; }
        public int maxUsers { get; set; }
    }

    public class GetPlayersInRoomRequest
    {
        public GetPlayersInRoomRequest(int id)
        {
            roomId = id;
        }
        public int roomId { get; set; }
    }

    enum codes
    {
        LOGIN_CODE = 100,
        SIGNUP_CODE,
        LOGOUT_CODE,
        JOIN_ROOM_CODE,
        CREATE_ROOM_CODE,
        GET_PLAYERS_CODE,
        GET_STATS_CODE,
        GET_ROOM_CODE,
        UPDATE_INGAME_STATE,
    }

    class Serializer
    {

        public static byte[] Serialize(user r)
        {
            byte[] msg = new byte[5];
            msg[0] = (byte)codes.UPDATE_INGAME_STATE;

            string data = JsonConvert.SerializeObject(r, Formatting.None);
            int len = data.Length;

            byte[] requestData = Encoding.ASCII.GetBytes(data);

            msg[1] = (byte)(len >> 24);
            msg[2] = (byte)(len >> 16);
            msg[3] = (byte)(len >> 8);
            msg[4] = (byte)len;

            byte[] msgToServer = new byte[msg.Length + len];
            msg.CopyTo(msgToServer, 0);
            requestData.CopyTo(msgToServer, msg.Length);

            return msgToServer;
        }

        public static byte[] Serialize(SignInRequest r)
        {
            byte[] msg = new byte[5];
            msg[0] = (byte)codes.LOGIN_CODE;

            string data = JsonConvert.SerializeObject(r, Formatting.None);
            int len = data.Length;

            byte[] requestData = Encoding.ASCII.GetBytes(data);

            msg[1] = (byte)(len >> 24);
            msg[2] = (byte)(len >> 16);
            msg[3] = (byte)(len >> 8);
            msg[4] = (byte)len;

            byte[] msgToServer = new byte[msg.Length + len];
            msg.CopyTo(msgToServer, 0);
            requestData.CopyTo(msgToServer, msg.Length);

            return msgToServer;
        }

        public static byte[] Serialize(SignUpRequest r)
        {
            byte[] msg = new byte[5];
            msg[0] = (byte)codes.SIGNUP_CODE;



            string data = JsonConvert.SerializeObject(r, Formatting.None);
            int len = data.Length;

            byte[] requestData = Encoding.ASCII.GetBytes(data);

            msg[1] = (byte)(len >> 24);
            msg[2] = (byte)(len >> 16);
            msg[3] = (byte)(len >> 8);
            msg[4] = (byte)len;

            byte[] msgToServer = new byte[msg.Length + len];
            msg.CopyTo(msgToServer, 0);
            requestData.CopyTo(msgToServer, msg.Length);

            return msgToServer;
        }

        public static byte[] Serialize(JoinRoomRequest r)
        {
            byte[] msg = new byte[5];
            msg[0] = (byte)codes.JOIN_ROOM_CODE;



            string data = JsonConvert.SerializeObject(r, Formatting.None);
            int len = data.Length;

            byte[] requestData = Encoding.ASCII.GetBytes(data);

            msg[1] = (byte)(len >> 24);
            msg[2] = (byte)(len >> 16);
            msg[3] = (byte)(len >> 8);
            msg[4] = (byte)len;

            byte[] msgToServer = new byte[msg.Length + len];
            msg.CopyTo(msgToServer, 0);
            requestData.CopyTo(msgToServer, msg.Length);

            return msgToServer;
        }

        public static byte[] Serialize(CreateRoomRequest r)
        {
            byte[] msg = new byte[5];
            msg[0] = (byte)codes.CREATE_ROOM_CODE;



            string data = JsonConvert.SerializeObject(r, Formatting.None);
            int len = data.Length;

            byte[] requestData = Encoding.ASCII.GetBytes(data);

            msg[1] = (byte)(len >> 24);
            msg[2] = (byte)(len >> 16);
            msg[3] = (byte)(len >> 8);
            msg[4] = (byte)len;

            byte[] msgToServer = new byte[msg.Length + len];
            msg.CopyTo(msgToServer, 0);
            requestData.CopyTo(msgToServer, msg.Length);

            return msgToServer;
        }

        public static byte[] Serialize(GetPlayersInRoomRequest r)
        {
            byte[] msg = new byte[5];
            msg[0] = (byte)codes.GET_PLAYERS_CODE;



            string data = JsonConvert.SerializeObject(r, Formatting.None);
            int len = data.Length;

            byte[] requestData = Encoding.ASCII.GetBytes(data);

            msg[1] = (byte)(len >> 24);
            msg[2] = (byte)(len >> 16);
            msg[3] = (byte)(len >> 8);
            msg[4] = (byte)len;

            byte[] msgToServer = new byte[msg.Length + len];
            msg.CopyTo(msgToServer, 0);
            requestData.CopyTo(msgToServer, msg.Length);

            return msgToServer;
        }

        public static byte[] serializeLogOut()
        {
            byte[] msg = new byte[5];
            msg[0] = Convert.ToByte(codes.LOGOUT_CODE);
            return msg;
        }

        public static byte[] serializeStats()
        {
            byte[] msg = new byte[5];
            msg[0] = Convert.ToByte(codes.GET_STATS_CODE);
            return msg;
        }

        public static byte[] serializeRooms()
        {
            byte[] msg = new byte[5];
            msg[0] = Convert.ToByte(codes.GET_ROOM_CODE);
            return msg;
        }

    }
}
