﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json;

namespace UnderWorld.Backend
{
    public class Respone
    {
        public int status { get; set; }
    }





    public class getRoomPlayerStates : Respone
    {

    }



    public class SignInResponse : Respone
    {
        
    }

    public class SignUpResponse : Respone
    {
       
    }

    public class LogOutResponse : Respone
    {
        
    }

    public class JoinRoomResponse : Respone
    {

    }

    public class CreateRoomRespone : Respone
    {

    }

    public class GetRoomsRespone : Respone
    {
        public string rooms { get; set; }
    }

    public class RoomsNameRespone : Respone
    {
        public RoomsNameRespone(int status, string[] names)
        {
            this.status = status;
            rooms = names;
        }
        public string[] rooms { get; set; }
    }
    
    public class AllStatsRespone : Respone
    { 
        public string UserStatisics { get; set; }
        public string HighScore { get; set; }
    }

    public class UserStats
    {
        public UserStats(double avgTime, int numCorrect, int totalAns, int numGames)
        {
            this.avgTime = avgTime;
            this.numCorrect = numCorrect;
            this.totalAns = totalAns;
            this.numGames = numGames;
        }
        public double avgTime { get; set; }
        public int numCorrect { get; set; }
        public int totalAns { get; set; }
        public int numGames { get; set; }
    }

    public class HighScore
    {
        public Tuple<string, double> FirstUser { get; set; }
        public Tuple<string, double> SecondUser { get; set; }
        public Tuple<string, double> ThirdUser { get; set; }
    }

    public class StatisticsRespone : Respone
    {
        public StatisticsRespone(int status, UserStats self, HighScore top)
        {
            this.status = status;
            this.self = self;
            this.top = top;
        }
        public UserStats self;
        public HighScore top;
    }

    public class PlayersInRoomRespone : Respone
    {
        public string PlayersInRoom { get; set; }

        public string Name { get; set; }

        public int MaxPlayers { get; set; }

        public int QuestionNum { get; set; }

        public int QuestionTime { get; set; }
    }




    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Layer
    {
        public List<int> data { get; set; }
        public int height { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public int opacity { get; set; }
        public string type { get; set; }
        public bool visible { get; set; }
        public int width { get; set; }
        public int x { get; set; }
        public int y { get; set; }
    }

    public class Tileset
    {
        public int firstgid { get; set; }
        public string source { get; set; }
    }

    public class MapJson
    {
        public int compressionlevel { get; set; }
        public int height { get; set; }
        public bool infinite { get; set; }
        public List<Layer> layers { get; set; }
        public int nextlayerid { get; set; }
        public int nextobjectid { get; set; }
        public string orientation { get; set; }
        public string renderorder { get; set; }
        public string tiledversion { get; set; }
        public int tileheight { get; set; }
        public List<Tileset> tilesets { get; set; }
        public int tilewidth { get; set; }
        public string type { get; set; }
        public double version { get; set; }
        public int width { get; set; }
    }




    public class PlayersInRoomNames : Respone
    {
        public PlayersInRoomNames(int status, string[] players, string name, int max, int questionNum, int questionTime)
        {
            this.status = status;
            this.players = players;
            this.Name = name;
            this.QuestionNum = questionNum;
            this.QuestionTime = questionTime;
            this.MaxPlayers = max;
        }
        public string[] players { get; set; }

        public string Name { get; set; }

        public int MaxPlayers { get; set; }

        public int QuestionNum { get; set; }

        public int QuestionTime { get; set; }
    }


    public class playersStatusInGame : Respone
    {
        public string PlayersInRoom { get; set; }
    }




    class ErrorResponse
    {
        public string message { get; set; }
    }

    class Deserializer
    {
        public static playersStatusInGame DeserializePlayerStatus(byte[] data)
        {
            //MessageBox.Show(System.Text.Encoding.ASCII.GetString(data));
            return JsonConvert.DeserializeObject<playersStatusInGame>(Encoding.ASCII.GetString(data));
        }
        public static SignInResponse DeserializeSignInResponse(byte[] data)
        {
            //MessageBox.Show(System.Text.Encoding.ASCII.GetString(data));
            return JsonConvert.DeserializeObject<SignInResponse>(Encoding.ASCII.GetString(data));
        }

        public static SignUpResponse DeserializeSignUpResponse(byte[] data)
        {
            //MessageBox.Show(System.Text.Encoding.ASCII.GetString(data));
            return JsonConvert.DeserializeObject<SignUpResponse>(Encoding.ASCII.GetString(data));
        }

        public static LogOutResponse DeserializeLogOutResponse(byte[] data)
        {
            //MessageBox.Show(System.Text.Encoding.ASCII.GetString(data));
            return JsonConvert.DeserializeObject<LogOutResponse>(Encoding.ASCII.GetString(data));
        }

        public static JoinRoomResponse DeserializeJoinRoomResponse(byte[] data)
        {
            //MessageBox.Show(System.Text.Encoding.ASCII.GetString(data));
            return JsonConvert.DeserializeObject<JoinRoomResponse>(Encoding.ASCII.GetString(data));
        }

        public static CreateRoomRespone DeserializeCreateRoomResponse(byte[] data)
        {
            //MessageBox.Show(System.Text.Encoding.ASCII.GetString(data));
            return JsonConvert.DeserializeObject<CreateRoomRespone>(Encoding.ASCII.GetString(data));
        }


        public static playersStatusInGame DeserializeUsersStatsInGame(byte[] data)
        {
            //MessageBox.Show(System.Text.Encoding.ASCII.GetString(data));
            return JsonConvert.DeserializeObject<playersStatusInGame>(Encoding.ASCII.GetString(data));
        }


        public static RoomsNameRespone DeserializeGetRoomsRespones(byte[] data)
        {
            GetRoomsRespone roomsR = JsonConvert.DeserializeObject<GetRoomsRespone>(Encoding.ASCII.GetString(data));
            string namesStr = roomsR.rooms;
            string[] names = namesStr.Split(',');
            RoomsNameRespone roomsNames = new RoomsNameRespone(roomsR.status, names );
            return roomsNames;
        }

        public static StatisticsRespone DeserializeGetStatsRespones(byte[] data)
        {
            AllStatsRespone allStats = JsonConvert.DeserializeObject<AllStatsRespone>(Encoding.ASCII.GetString(data));
            string[] selfS = allStats.UserStatisics.Split(',');
            UserStats userStats = new UserStats(Convert.ToDouble(selfS[3]), Convert.ToInt32(selfS[2]), Convert.ToInt32(selfS[1]), Convert.ToInt32(selfS[0]));
            string[] highScore = allStats.HighScore.Split(',');
            HighScore top3Score = new HighScore();
            string[] param = highScore[0].Split(':');
            top3Score.FirstUser = Tuple.Create<string, double>(param[0], Convert.ToDouble(param[1]));
            param = highScore[1].Split(':');
            top3Score.SecondUser = Tuple.Create<string, double>(param[0], Convert.ToDouble(param[1]));
            param = highScore[2].Split(':');
            top3Score.ThirdUser = Tuple.Create<string, double>(param[0], Convert.ToDouble(param[1]));
            StatisticsRespone statistics = new StatisticsRespone(allStats.status, userStats, top3Score);
            return statistics;
        }

        public static PlayersInRoomNames DeserializeGetPlayersInRoomRespone(byte[] data)
        {
            PlayersInRoomRespone playersInRoom = JsonConvert.DeserializeObject<PlayersInRoomRespone>(Encoding.ASCII.GetString(data));
            string[] names = playersInRoom.PlayersInRoom.Split(',');
            return new PlayersInRoomNames(playersInRoom.status, names, playersInRoom.Name, playersInRoom.MaxPlayers, playersInRoom.QuestionNum, playersInRoom.QuestionTime);
        }

        public static ErrorResponse DeserializeErrorResponse(byte[] data)
        {
            return JsonConvert.DeserializeObject<ErrorResponse>(Encoding.ASCII.GetString(data));
        }

        public static int getCode(byte[] data)
        {
            return (int)data[0];
        }

        public static int getLen(byte[] data)
        {
            return BitConverter.ToInt32(data, 1);
        }

    }
}
