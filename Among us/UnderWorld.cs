﻿using UnderWorld.Backend;
using UnderWorld.DrawableObjects;
using UnderWorld.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SharpDX.Direct2D1.Effects;
using SharpDX.DirectWrite;
using System;
using System.Collections.Generic;
using System.Linq;
using Penumbra;

namespace UnderWorld
{
    public class UnderWorld : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        Communicator comms;
        private StateManager _StateMan;
        PenumbraComponent _penumbra;



        public UnderWorld()
        {
            _graphics = new GraphicsDeviceManager(this);
            _graphics.PreferredBackBufferWidth = 960;
            _graphics.PreferredBackBufferHeight = 540;
            _graphics.ApplyChanges();
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            _penumbra = new PenumbraComponent(this);
            Components.Add(_penumbra);
            _penumbra.Visible = false;


        }

        protected override void Initialize()
        {
            this.comms = new Communicator();
            base.Initialize();
            _penumbra.Visible = false;

        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _StateMan = new StateManager(_spriteBatch, this, _graphics, comms, _penumbra);
        }

        protected override void Update(GameTime gameTime)
        {
            _StateMan.Update(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            _penumbra.BeginDraw();
            GraphicsDevice.Clear(Color.Black);
            _StateMan.Draw(gameTime);
            _penumbra.Draw(gameTime);
            //_StateMan.DrawAbove(gameTime);
            //base.Draw(gameTime);
        }
    }
}
