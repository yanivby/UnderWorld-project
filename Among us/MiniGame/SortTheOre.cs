﻿using UnderWorld.Backend;
using UnderWorld.DrawableObjects;
using UnderWorld.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using MonoGame.Penumbra.WindowsDX;
using Penumbra;
using UnderWorld.GameStates;
using UnderWorld.Controls;

namespace UnderWorld.MiniGame
{
    class SortTheOre : State
    {
        private Sprite _map;
        List<Component> components;
        string curToSort;
        int correctlySortedCount = 0;
        const int needToSort = 10;
        int curIdToSort;

        TextManager textMan;
        Random rand;

        public SortTheOre(StateManager man) :
        base(man)
        {
            Load();
        }
        public override void Load()
        {
            textMan = new TextManager(_game.Content.Load<SpriteFont>("Fonts/ButtonFont"));
            textMan.AddTextObject("TOSORTTTT", new Vector2 (80, this._graphics.PreferredBackBufferHeight / 2), "SortThing");
            rand = new Random();
            _map = new Sprite(this._game.Content.Load<Texture2D>("MiniGames\\BackMiniGame3"))
            {
                Color = Color.White,
                Position = new Vector2(0, 0),
                size = 100,

            };
            Button snawerium = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2((this._graphics.PreferredBackBufferWidth / 4) * 3+50, 100),
                text = "snawerium",
                PenColor = Color.Black,
                id = "snawerium"
            };

            Button copper = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2((this._graphics.PreferredBackBufferWidth / 4) * 3+50, 300),
                text = "copper",
                PenColor = Color.Black,
                id = "copper",

            };

            Button coal = new Button(this._game.Content.Load<Texture2D>("Controls/Button"),
                       _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
            {
                Position = new Vector2((this._graphics.PreferredBackBufferWidth / 4) * 3+ 50, 500),
                text = "coal",
                PenColor = Color.Black,
                id = "coal",
            };
            copper.click += clickSort;
            snawerium.click += clickSort;
            coal.click += clickSort; 

            components = new List<Component>()
            {
                copper,
                coal,
                snawerium
            };


            getRandomOre();




        }

        private void clickSort(object sender, EventArgs e)
        {
            Button cur = (Button)sender;

            if (cur.id == curToSort)
            {
                correctlySortedCount++;
                if (correctlySortedCount == needToSort)
                {
                    _StateManager.LastState();
                }
                getRandomOre();
            }
        }

        private void getRandomOre()
        {
            switch (rand.Next(0, 3))
            {
                case 0:
                    curToSort = "snawerium";
                    curIdToSort = 0;
                    break;
                case 1:
                    curToSort = "copper";
                    curIdToSort = 1;
                    break;
                case 2:
                    curToSort = "coal";
                    curIdToSort = 2;
                    break;
                default:
                    break;

            }
            textMan.UpdateSingle("SortThing", curToSort);
        }

        public override void update(GameTime gameTime)
        {
            foreach (var button in components)
            {
                button.Update(gameTime);
            }
            
        }
        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin(samplerState: SamplerState.PointClamp);

            _map.Draw(_spriteBatch);
            textMan.Draw(_spriteBatch);
            foreach (var single in components)
                single.Draw(_spriteBatch);
            _spriteBatch.End();

        }
    }
}
