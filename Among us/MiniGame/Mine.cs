﻿using UnderWorld.Backend;
using UnderWorld.DrawableObjects;
using UnderWorld.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using MonoGame.Penumbra.WindowsDX;
using Penumbra;
using UnderWorld.GameStates;
using UnderWorld.Controls;

namespace UnderWorld.MiniGame
{
    class Mine : State
    {
        private List<Button> _stuffs = new List<Button>();
        private Sprite _map;
        private const int startingScale = 30;
        private int pressedCount = 0;



        public Mine(StateManager man) : 
        base(man)
        {
            Load();
        }
        public override void Load()
        {
            var rand = new Random();
            _map = new Sprite(this._game.Content.Load<Texture2D>("MiniGames\\BackMiniGame"))
            {
                Color = Color.White,
                Position = new Vector2(0, 0),
                size = 150,

            };
            for (int i = 0; i < 6; i++)
            {
                switch (rand.Next(0, 3))
                {
                    case 0:
                        _stuffs.Add(new Button(this._game.Content.Load<Texture2D>("MiniGames\\coal"), _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
                        {
                            Position = new Vector2(rand.Next(64, this._StateManager._graphics.PreferredBackBufferWidth - 64),rand.Next(20, this._StateManager._graphics.PreferredBackBufferHeight - 64)),
                            scale = startingScale,
                        });
                        break;
                    case 1:
                        _stuffs.Add(new Button(this._game.Content.Load<Texture2D>("MiniGames\\copper"), _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
                        {
                            Position = new Vector2(rand.Next(64, this._StateManager._graphics.PreferredBackBufferWidth - 64), rand.Next(20, this._StateManager._graphics.PreferredBackBufferHeight - 64)),
                            scale = startingScale
                        });
                        break;
                    case 2:
                        _stuffs.Add(new Button(this._game.Content.Load<Texture2D>("MiniGames\\nut"), _game.Content.Load<SpriteFont>("Fonts/ButtonFont"))
                        {
                            Position = new Vector2(rand.Next(64, this._StateManager._graphics.PreferredBackBufferWidth - 64), rand.Next(64, this._StateManager._graphics.PreferredBackBufferHeight - 64)),
                            scale = startingScale
                        });
                        break;
                    default:
                        break;
                }
                _stuffs[i].id = i.ToString();
                _stuffs[i].click += Mine_click;
                
            }
        }

        private void Mine_click(object sender, EventArgs e)
        {
            Button cur = (Button)sender;
            {
                _stuffs[int.Parse(cur.id)].scale-=startingScale/3;
            }
            if (_stuffs[int.Parse(cur.id)].scale ==  0)
            {
                pressedCount++;
            }
             
            if (pressedCount == 6)
            {
                _StateManager.LastState();
            }

        }

        public override void update(GameTime gameTime)
        {
            foreach (var button in _stuffs)
            {
                button.Update(gameTime);
            }
        }
        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin(samplerState: SamplerState.PointClamp);

            _map.Draw(_spriteBatch);

            foreach (var single in _stuffs)
                single.Draw(_spriteBatch);
            _spriteBatch.End();

        }




    }
}
