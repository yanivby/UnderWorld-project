﻿using UnderWorld.Backend;
using UnderWorld.DrawableObjects;
using UnderWorld.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using MonoGame.Penumbra.WindowsDX;
using Penumbra;
using UnderWorld.GameStates;
using UnderWorld.Controls;

namespace UnderWorld.MiniGame
{
    class CatchTheOre : State
    {
        Random rand;

        Player catcher;
        private Stack<Sprite> falling = new Stack<Sprite>();
        private Stack<Sprite> waitingToFall = new Stack<Sprite>();
        private const int fallingAtATime = 3;
        private const int startingAmount = 10;
        private const int startingScale = 200;
        private int Cought = 0;
        private Sprite _map;
        private float speed;
        Dictionary<string, Animation> animations;
        private const int GameObjectsScaling = 200;

        public CatchTheOre(StateManager man) :
        base(man)
        {
            Load();
        }
        

        public override void Load()
        {
            rand = new Random();
            List<Rectangle> collidables = new List<Rectangle>();
            collidables.Add(new Rectangle(-1, 0, 1, _graphics.PreferredBackBufferHeight));
            collidables.Add(new Rectangle(_graphics.PreferredBackBufferWidth, 0, _graphics.PreferredBackBufferWidth +1, _graphics.PreferredBackBufferHeight));

            _map = new Sprite(this._game.Content.Load<Texture2D>("MiniGames\\BackMiniGame"))
            {
                Color = Color.White,
                Position = new Vector2(0, 0),
                size = 150,

            };



            animations = new Dictionary<string, Animation>()
            {
                { "WalkUp", new Animation(_game.Content.Load<Texture2D>("Animations/WalkingUp"), 4, Color.White)},
                { "WalkDown", new Animation(_game.Content.Load<Texture2D>("Animations/WalkingDown"), 4, Color.White)},
                { "WalkLeft", new Animation(_game.Content.Load<Texture2D>("Animations/WalkingLeft"), 4, Color.White)},
                { "WalkRight", new Animation(_game.Content.Load<Texture2D>("Animations/WalkingRight"), 4, Color.White)},
                { "StopLeft", new Animation(_game.Content.Load<Texture2D>("Animations/StopLeft"), 1, Color.White)},
                { "StopRight", new Animation(_game.Content.Load<Texture2D>("Animations/StopRight"), 1, Color.White)},

            };
            for (int i = 0; i < startingAmount; i++)
            {
                int xVal = rand.Next(2 * animations["WalkUp"].FrameWidth, (_graphics.PreferredBackBufferWidth - 2 * animations["WalkUp"].FrameWidth));
                int yVal = rand.Next(-100, 0);
                int RandVal = rand.Next(rand.Next(0, 3));
                switch (rand.Next(0, 3))
                {
                    case 0:
                        waitingToFall.Push(new Sprite(this._game.Content.Load<Texture2D>("MiniGames\\coal"))
                        {
                            Position = new Vector2(xVal, yVal),
                            size = startingScale,
                            id = i
                        });
                        break;
                    case 1:
                        waitingToFall.Push(new Sprite(this._game.Content.Load<Texture2D>("MiniGames\\copper"))
                        {
                            Position = new Vector2(xVal, yVal),
                            size = startingScale,
                            id = i
                        });
                        break;
                    case 2:
                        waitingToFall.Push(new Sprite(this._game.Content.Load<Texture2D>("MiniGames\\nut"))
                        {
                            Position = new Vector2(xVal, yVal),
                            size = startingScale,
                            id = i
                        });
                        break;
                    default:
                        break;
                }

            }
            catcher = new Player(animations)
            {
                InputKeys = new Input()
                {
                    left = Keys.A,
                    right = Keys.D,
                },
                Position = new Vector2(_graphics.PreferredBackBufferWidth/2, _graphics.PreferredBackBufferHeight-3*animations["WalkUp"].FrameHeight),
                Color = Color.Blue,
                speed = 8,
                size = GameObjectsScaling,
                isUser = true,
                Collision = collidables,
            };
        }

        public override void update(GameTime gameTime)
        {
            catcher.Update(gameTime);
            
            if (falling.Count < fallingAtATime)
            {
                if(waitingToFall.Count > 0)
                {
                    falling.Push(waitingToFall.Pop());
                }
            }


            Stack<Sprite> temp = new Stack<Sprite>();
            while(falling.Count >0 )
            {
                Sprite tempSprite = falling.Pop();
                tempSprite.Position = new Vector2(tempSprite.Position.X, tempSprite.Position.Y + 5);
                if (tempSprite.Position.Y > _graphics.PreferredBackBufferHeight)
                {
                    int yVal = rand.Next(-100, 0);
                    int xVal = rand.Next(2 * animations["WalkUp"].FrameWidth, (_graphics.PreferredBackBufferWidth - 2 * animations["WalkUp"].FrameWidth));

                    tempSprite.Position = new Vector2(xVal, yVal);
                    waitingToFall.Push(tempSprite);
                }
                else
                {
                    if (catcher.Rectangle.Intersects(tempSprite.Rectangle))
                    {
                        Cought++;
                    }
                    else
                    {
                        temp.Push(tempSprite);
                    }
                }
            }
            while(temp.Count > 0 )
            {
                falling.Push(temp.Pop());

            }

            if (Cought == startingAmount)
            {
                _StateManager.ChangeState(States.INGAME);
            }
        }
        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin(samplerState: SamplerState.PointClamp);
            _map.Draw(_spriteBatch);
            Stack<Sprite> temp = new Stack<Sprite>();
            while (falling.Count > 0)
            {
                Sprite tempSprite = falling.Pop();
                tempSprite.Draw(_spriteBatch);
                temp.Push(tempSprite);

            }
            while (temp.Count > 0)
            {
                falling.Push(temp.Pop());

            }
            catcher.Draw(_spriteBatch);
            _spriteBatch.End();

        }

    }
}
