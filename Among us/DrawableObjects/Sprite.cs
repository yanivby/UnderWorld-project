﻿using UnderWorld.Backend;
using UnderWorld.Managers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnderWorld.DrawableObjects
{
    public class Sprite : Component
    {
        #region properties
        protected Texture2D _texture;
        public AnimationManager _animationManager;
        public Dictionary<string, Animation> _animations;
        protected Vector2 _position;
        public bool isImpostor = false;

        public List<Rectangle> Collision { set; get; }
        public int animation;
        public Vector2 Position
        {
            get { return _position; }
            set
            {
                _position = value;
                if (_animationManager != null)
                    _animationManager.Position = _position;
            }
        }
        public float SpritHeight { get { if (_animationManager != null) return _animationManager.AnimationHeight; else return 0; } }
        public float SpritWidth { get { if (_animationManager != null) return _animationManager.AnimationWidth; else return 0; } }


        public int alive = 1;
        public Vector2 Velocity;
        public Color color = Color.White;
        public Color Color { get { return color; } set { if (_animationManager != null) { _animationManager.setColor(value); } color = value; } }

        public float speed;
        public Input InputKeys;
        private int _size;
        public int size { 
            set 
            { 
            if (_animationManager != null)
                {
                    _animationManager.scale = value;
                }
                _size = value;
            }
            get
            {
                return _size;
            }
        
        }
        public bool isUser = false;
        public int id = 0;
        public string name = "";
        public bool FacingRight = true;
        public bool showName = false;
        #endregion

        #region methods
        public Rectangle Rectangle
        {
            get
            {
                if (_animationManager != null)
                {
                    return new Rectangle((int)Position.X, (int)Position.Y, _animationManager._rectangle.Width, _animationManager._rectangle.Height);
                }
                if (_texture != null)
                    return new Rectangle((int)Position.X, (int)Position.Y, _texture.Width * size / 100, _texture.Height * size / 100);
                else
                    return new Rectangle();
            }
            set {; }
        }
        public Sprite(Texture2D texture)
        {
            _texture = texture;
            size = 100;
        }
        public Sprite(Dictionary<string, Animation> animations)
        {
            _animations = animations;
            _animationManager = new AnimationManager(_animations.First().Value);
            size = 100;

        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (this.alive == 1)
            {
                if (_texture != null)
                    spriteBatch.Draw(_texture, Rectangle, Color);
                else if (_animationManager != null)
                {
                    AnimationsSelctor();
                    _animationManager.Draw(spriteBatch);
                }
                else throw new Exception("ERROR NO TEXTURE");
            }
              
        }

        public override void Update(GameTime gameTime )
        {
            throw new NotImplementedException();
        }
        
        protected virtual void AnimationsSelctor()
        {
            if (isUser)
            {
                if (Velocity.X > 0)
                {
                    _animationManager.Play(_animations["WalkRight"]);
                    FacingRight = true;
                    animation = 2;
                }
                else if (Velocity.X < 0)
                {
                    _animationManager.Play(_animations["WalkLeft"]);
                    FacingRight = false;
                    animation = 3;
                }
                else if (Velocity.Y > 0 || Velocity.Y < 0)
                    if (FacingRight)
                    {
                        _animationManager.Play(_animations["WalkRight"]);
                        animation = 2;
                    }
                    else
                    {
                        _animationManager.Play(_animations["WalkLeft"]);
                        animation = 3;
                    }
                else
                {
                    if (FacingRight)
                    {
                        _animationManager.Play(_animations["StopRight"]);
                        animation = 0;
                    }
                    else
                    {
                        _animationManager.Play(_animations["StopLeft"]);
                        animation = 1;
                    }
                }
            }
            else
                switch (animation)
                {
                    case 0:
                        _animationManager.Play(_animations["StopRight"]);
                        break;

                    case 1:
                        _animationManager.Play(_animations["StopLeft"]);
                        break;

                    case 2:
                        _animationManager.Play(_animations["WalkRight"]);
                        break;

                    case 3:
                        _animationManager.Play(_animations["WalkLeft"]);
                        break;



                    default:
                        _animationManager.Play(_animations["StopRight"]);

                        break;
                }
        }

        #region Colloision
        protected bool IsTouchingLeft(Rectangle rect)
        {
            return Rectangle.Right + Velocity.X > rect.Left &&
              Rectangle.Left < rect.Left &&
              Rectangle.Bottom > rect.Top &&
              Rectangle.Top < rect.Bottom;
        }

        protected bool IsTouchingRight(Rectangle rect)
        {
            return Rectangle.Left + Velocity.X < rect.Right &&
              Rectangle.Right > rect.Right &&
              Rectangle.Bottom > rect.Top &&
              Rectangle.Top < rect.Bottom;
        }

        protected bool IsTouchingTop(Rectangle rect)
        {
            return Rectangle.Bottom + Velocity.Y > rect.Top &&
              Rectangle.Top < rect.Top &&
              Rectangle.Right > rect.Left &&
              Rectangle.Left < rect.Right;
        }

        protected bool IsTouchingBottom(Rectangle rect)
        {
            return Rectangle.Top + Velocity.Y < rect.Bottom &&
              Rectangle.Bottom > rect.Bottom &&
              Rectangle.Right > rect.Left &&
              Rectangle.Left < rect.Right;
        }


        #endregion
        #endregion
    }
    public class Input
    {
        public Keys up;
        public Keys down;
        public Keys left;
        public Keys right;
        public Keys interact;
    }
}
