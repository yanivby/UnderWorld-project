﻿using UnderWorld.Backend;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnderWorld.DrawableObjects
{
    public class Player : Sprite
    {
        public event EventHandler interact;
        public Player(Texture2D texture) : base(texture)
        {

        }
        public Player(Dictionary<string, Animation> animations) : base(animations)
        {

        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (this.alive == 1)
                base.Draw(spriteBatch);

        }
        public override void Update(GameTime gameTime)
        {

            _animationManager.Update(gameTime);
            if (isUser)
            {
                
                    Move();
                if (Collision != null)
                {
                    foreach (var rect in Collision)
                    {

                        if (Velocity.X > 0 && IsTouchingLeft(rect) ||
                            Velocity.X < 0 && IsTouchingRight(rect))
                            Velocity.X = 0;
                        if (Velocity.Y > 0 && IsTouchingTop(rect) ||
                           Velocity.Y < 0 && IsTouchingBottom(rect))
                            Velocity.Y = 0;
                    }
                }
                
                Position += Velocity;

            }
        }

        private void Move()
        {
            if (isUser)
            {


                float maxSpeed = speed;
                float acceleration = speed / 4;
                float slowdownSpeed = speed / 16;

                if (Keyboard.GetState().IsKeyDown(InputKeys.interact))
                {
                    if (interact != null)
                    {
                        interact?.Invoke(this, new EventArgs());
                    }
                }


                if (Keyboard.GetState().IsKeyDown(InputKeys.up))
                    if (Velocity.Y >= -maxSpeed)
                        Velocity.Y += -acceleration;

                if (Keyboard.GetState().IsKeyDown(InputKeys.down))
                    if (Velocity.Y <= maxSpeed)
                        Velocity.Y += acceleration;

                if (Keyboard.GetState().IsKeyDown(InputKeys.left))
                    if (Velocity.X >= -maxSpeed)
                        Velocity.X += -acceleration;

                if (Keyboard.GetState().IsKeyDown(InputKeys.right))
                    if (Velocity.X <= maxSpeed)
                        Velocity.X += acceleration;

                if (Velocity.X > 0)
                    Velocity.X -= slowdownSpeed;
                if (Velocity.Y > 0)
                    Velocity.Y -= slowdownSpeed;
                if (Velocity.X < 0)
                    Velocity.X += slowdownSpeed;
                if (Velocity.Y < 0)
                    Velocity.Y += slowdownSpeed;
            }
        }
    }
}
