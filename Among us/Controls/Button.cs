﻿using UnderWorld.Backend;
using UnderWorld.DrawableObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SharpDX.Direct3D9;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using SharpDX.Direct2D1.Effects;

namespace UnderWorld.Controls
{
    public class Button : Backend.Component
    {
        #region fields

        private MouseState _currentMouse;
        private SpriteFont _font;
        private bool _isHovering;
        private MouseState _previousMouse;
        private Texture2D _texture;

        #endregion
        #region Properties

        public event EventHandler click;
        public event EventHandler click_not_selected;

        public int scale = 10;

        public Vector2 textLength { get { return _font.MeasureString(text); } }
        public string id = "";
        public bool Clicked { get; private set; }
        public Color PenColor { get; set; }
        public Vector2 Position { get; set; }
        public Rectangle Rectangle
        {
            get
            {
                return new Rectangle (
                    (int)(Position.X - _texture.Width/2), 
                    (int)(Position.Y - _texture.Height / 2), 
                    (_texture.Width / 10) * scale,
                    (_texture.Height / 10) * scale);
            }
        }
        public string text { get; set; }
        
        #endregion

        #region  methods

        public Button(Texture2D texture, SpriteFont font)
        {
            _texture = texture;
            _font = font;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            var color = Color.White;

            if (_isHovering)
            {
                color = Color.Gray;
            }

            spriteBatch.Draw(_texture, Rectangle, color);

            if (!string.IsNullOrEmpty(text))
            {
                var x = (Rectangle.X + (Rectangle.Width / 2)) - (_font.MeasureString(text).X / 2);
                var y = (Rectangle.Y + (Rectangle.Height / 2)) - (_font.MeasureString(text).Y / 2);

                spriteBatch.DrawString(_font, text, new Vector2(x, y), PenColor);
            }
        }

        public override void Update(GameTime gameTime)
        {
            _previousMouse = _currentMouse;
            _currentMouse = Mouse.GetState();

            var mouseRectangele = new Rectangle(_currentMouse.X, _currentMouse.Y, 1, 1);
            _isHovering = false;
            if (mouseRectangele.Intersects(Rectangle))
            {
                _isHovering = true;
                if (_currentMouse.LeftButton == ButtonState.Released && _previousMouse.LeftButton == ButtonState.Pressed)
                {
                    click?.Invoke(this, new EventArgs());
                }
            }
            else
            {
                if (_currentMouse.LeftButton == ButtonState.Released && _previousMouse.LeftButton == ButtonState.Pressed)
                {
                    if (click_not_selected != null)
                    {
                        click_not_selected?.Invoke(this, new EventArgs());
                    }
                }
            }
           
        }
        #endregion

    }
}
