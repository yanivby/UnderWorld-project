﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace UnderWorld.Controls
{
    class TextBox : Button
    {
        private Game _game;
        private bool _selected;
        public event EventHandler EnterPressed;
        public bool numbersOnly = false;

        public TextBox(Texture2D texture, SpriteFont font, Game game ) :  base(texture,font) 
        {
            _selected = false;
            _game = game;
            this.click += TextBox_click;
            this.click_not_selected += TextBox_click_not_selected;
            _game.Window.TextInput += TextInputHandler;

        }

        ~TextBox()
        {
           /* try
            {
                if (_game!=null)
                {
                    if (_game.Window!= null)
                    {
                        _game.Window.TextInput -= TextInputHandler;

                    }

                }

            }
            catch 
            {

          
            }*/
        }

        private void TextBox_click_not_selected(object sender, EventArgs e) //textbox not selected
        {
            _selected = false;
        }

        private void TextBox_click(object sender, EventArgs e) //textbox sellected
        {
            _selected = true;
        }


        private void TextInputHandler(object sender, TextInputEventArgs args)
        {
            var pressedKey = args.Key;
            var character = args.Character;
            if (_selected)
            {
                if (pressedKey == Microsoft.Xna.Framework.Input.Keys.Back)
                {
                    if (text.Length >0)
                    {
                        base.text = text.Substring(0, text.Length - 1);
                    }
                }else if(pressedKey == Microsoft.Xna.Framework.Input.Keys.Enter)
                {
                    if (EnterPressed != null)
                    {
                        EnterPressed?.Invoke(this, new EventArgs());
                    }
                }
                else if (numbersOnly)
                {
                    if (Char.IsDigit(character))
                    {
                        if (base.textLength.X < base.Rectangle.Width)
                            base.text += character;
                    }
                }
                else if (Char.IsLetter(character))
                {
                    if (base.textLength.X < base.Rectangle.Width)
                        base.text += character;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }


    }
}
