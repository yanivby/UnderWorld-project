﻿using System;

namespace UnderWorld
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new UnderWorld())
                game.Run();
        }
    }
}
